import 'package:flutter/material.dart';

const textMedium17 = TextStyle(fontSize: 17,fontWeight:FontWeight.w500);
const textRegular17 = TextStyle(fontSize: 17,fontWeight:FontWeight.w400);
const textRegular14 = TextStyle(fontSize: 14,fontWeight:FontWeight.w400);
const textMedium12 = TextStyle(fontSize: 12,fontWeight:FontWeight.w500);
const textMedium18 = TextStyle(fontSize: 18,fontWeight:FontWeight.w500);
const textBold36 = TextStyle(fontSize: 36,fontWeight:FontWeight.w600);
const textBold18 = TextStyle(fontSize: 18,fontWeight:FontWeight.w600);
const textBold24 = TextStyle(fontSize: 24,fontWeight:FontWeight.w600);
const textBold14 = TextStyle(fontSize: 14,fontWeight:FontWeight.w600);
const textBold13 = TextStyle(fontSize: 13,fontWeight:FontWeight.w600);