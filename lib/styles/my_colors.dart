import 'dart:ui';

class MyColors{
  static const disable = Color(0xffB3BFCB);
  static const titleHind = Color(0xff6A798A);
  static const fillTextField = Color(0xffEFF2F5);
  static const black = Color(0xff000000);
  static const orange = Color(0xffEA985B);
}