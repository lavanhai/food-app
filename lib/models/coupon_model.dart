class CouponModel {
  CouponModel({
    String? id,
    String? discount,
    String? quantity,
    Map<String, dynamic>? users,
  }) {
    _id = id;
    _discount = discount;
    _quantity = quantity;
    _users = users;
  }

  CouponModel.fromJson(dynamic json) {
    _id = json['id'];
    _discount = json['discount'];
    _quantity = json['quantity'];
    _users = json['users'];
  }

  String? _id;
  String? _discount;
  String? _quantity;
  Map<String, dynamic>? _users;

  CouponModel copyWith({
    String? id,
    String? discount,
    String? quantity,
    Map<String, dynamic>? users,
  }) =>
      CouponModel(
        id: id ?? _id,
        discount: discount ?? _discount,
        quantity: quantity ?? _quantity,
        users: users ?? _users,
      );

  String? get id => _id;

  String? get discount => _discount;

  String? get quantity => _quantity;

  Map<String, dynamic>? get users => _users;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['discount'] = _discount;
    map['quantity'] = _quantity;
    map['users'] = _users;
    return map;
  }
}
