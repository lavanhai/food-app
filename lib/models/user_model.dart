class UserModel {
  UserModel({
    String? id,
    String? fullName,
    String? phone,
    String? password,
    String? email,
    String? address,
    num? role,
    String? token
  }) {
    _id = id;
    _fullName = fullName;
    _phone = phone;
    _password = password;
    _email = email;
    _address = address;
    _role = role;
    _token = token;
  }

  UserModel.fromJson(dynamic json) {
    _id = json['user_id'];
    _token = json['token'];
    _fullName = json['fullName'];
    _phone = json['phone_number'];
    _password = json['password'];
    _address = json['address'];
    _email = json['email'];
    _role = json['role'] == "admin" ? 1 : 0;
  }

  String? _id;
  String? _token;
  String? _fullName;
  String? _phone;
  String? _password;
  String? _email;
  String? _address;
  num? _role;

  UserModel copyWith({
    String? id,
    String? fullName,
    String? phone,
    String? password,
    String? email,
    String? address,
    num? role,
    String? token
  }) =>
      UserModel(
        id: id ?? _id,
        token: _token ?? _token,
        fullName: fullName ?? _fullName,
        phone: phone ?? _phone,
        password: password ?? _password,
        email: email ?? _email,
        address: address ?? _address,
        role: role ?? _role,
      );

  String? get id => _id;
  String? get token => _token;

  String? get fullName => _fullName;

  String? get phone => _phone;

  String? get password => _password;
  String? get email => _email;
  String? get address => _address;

  num? get role => _role;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['token'] = _token;
    map['fullName'] = _fullName;
    map['phone'] = _phone;
    map['password'] = _password;
    map['email'] = _email;
    map['address'] = _address;
    map['role'] = _role;
    return map;
  }
}
