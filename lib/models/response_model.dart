class ResponseModel {
  ResponseModel({
      num? code, 
      dynamic data,
      String? message,}){
    _code = code;
    _data = data;
    _message = message;
}

  ResponseModel.fromJson(dynamic json) {
    _code = json['code'];
    _data = json['data'];
    _message = json['message'];
  }
  num? _code;
  dynamic _data;
  String? _message;
ResponseModel copyWith({  num? code,
  dynamic data,
  String? message,
}) => ResponseModel(  code: code ?? _code,
  data: data ?? _data,
  message: message ?? _message,
);
  num? get code => _code;
  dynamic get data => _data;
  String? get message => _message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['data'] = _data;
    map['message'] = _message;
    return map;
  }

}