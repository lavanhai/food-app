class CartModel {
  CartModel({
      Map<String, dynamic>? product,
      String? quantity,
      String? id,}){
    _product = product;
    _quantity = quantity;
    _id = id;
}

  CartModel.fromJson(dynamic json) {
    _product = json['product'] != null ? json['product'] : null;
    _quantity = json['quantity'];
    _id = json['id'];
  }
  Map<String, dynamic>? _product;
  String? _quantity;
  String? _id;
CartModel copyWith({  Map<String, dynamic>? product,
  String? quantity,
  String? id,
}) => CartModel(  product: product ?? _product,
  quantity: quantity ?? _quantity,
  id: id ?? _id,
);
  Map<String, dynamic>? get product => _product;
  String? get quantity => _quantity;
  String? get id => _id;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['product'] = _product;
    map['quantity'] = _quantity;
    map['id'] = _id;
    return map;
  }

}