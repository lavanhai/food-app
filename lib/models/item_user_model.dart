class ItemUserModel {
  ItemUserModel({
      String? userId, 
      String? name, 
      String? email, 
      String? role, 
      String? phoneNumber, 
      String? address,}){
    _userId = userId;
    _name = name;
    _email = email;
    _role = role;
    _phoneNumber = phoneNumber;
    _address = address;
}

  ItemUserModel.fromJson(dynamic json) {
    _userId = json['user_id'];
    _name = json['name'];
    _email = json['email'];
    _role = json['role'];
    _phoneNumber = json['phone_number'];
    _address = json['address'];
  }
  String? _userId;
  String? _name;
  String? _email;
  String? _role;
  String? _phoneNumber;
  String? _address;
ItemUserModel copyWith({  String? userId,
  String? name,
  String? email,
  String? role,
  String? phoneNumber,
  String? address,
}) => ItemUserModel(  userId: userId ?? _userId,
  name: name ?? _name,
  email: email ?? _email,
  role: role ?? _role,
  phoneNumber: phoneNumber ?? _phoneNumber,
  address: address ?? _address,
);
  String? get userId => _userId;
  String? get name => _name;
  String? get email => _email;
  String? get role => _role;
  String? get phoneNumber => _phoneNumber;
  String? get address => _address;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['user_id'] = _userId;
    map['name'] = _name;
    map['email'] = _email;
    map['role'] = _role;
    map['phone_number'] = _phoneNumber;
    map['address'] = _address;
    return map;
  }

}