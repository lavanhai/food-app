class ProductModel {
  ProductModel({
      String? id, 
      String? productName, 
      String? description, 
      String? image,
      String? price,}){
    _id = id;
    _productName = productName;
    _description = description;
    _price = price;
    _image = image;
}

  ProductModel.fromJson(dynamic json) {
    _id = json['id'];
    _productName = json['productName'];
    _description = json['description'];
    _price = json['price'];
    _image = json['image'];
  }
  String? _id;
  String? _productName;
  String? _description;
  String? _price;
  String? _image;
ProductModel copyWith({  String? id,
  String? productName,
  String? description,
  String? price,
  String? image,
}) => ProductModel(  id: id ?? _id,
  productName: productName ?? _productName,
  description: description ?? _description,
  price: price ?? _price,
  image: image ?? _image,
);
  String? get id => _id;
  String? get productName => _productName;
  String? get description => _description;
  String? get price => _price;
  String? get image => _image;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['productName'] = _productName;
    map['description'] = _description;
    map['price'] = _price;
    map['image'] = _image;
    return map;
  }

}