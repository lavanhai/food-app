class OrderModel {
  final String totalPayment;
  final String discount;
  final String id;
  final DateTime paymentTime;
  final List<ProductOrder> products;

  OrderModel({
    required this.totalPayment,
    required this.discount,
    required this.id,
    required this.paymentTime,
    required this.products,
  });

  factory OrderModel.fromJson(Map<String, dynamic> json) {
    return OrderModel(
      totalPayment: json['totalPayment'],
      discount: json['discount'],
      id: json['id'],
      paymentTime: DateTime.parse(json['paymentTime']),
      products: List<ProductOrder>.from(json['products'].map((productJson) => ProductOrder.fromJson(productJson))),
    );
  }
}

class ProductOrder {
  final Product product;
  final String quantity;
  final String id;

  ProductOrder({
    required this.product,
    required this.quantity,
    required this.id,
  });

  factory ProductOrder.fromJson(Map<String, dynamic> json) {
    return ProductOrder(
      product: Product.fromJson(json['product']),
      quantity: json['quantity'],
      id: json['id'],
    );
  }
}

class Product {
  final String image;
  final String price;
  final String description;
  final String id;
  final String productName;

  Product({
    required this.image,
    required this.price,
    required this.description,
    required this.id,
    required this.productName,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      image: json['image'],
      price: json['price'],
      description: json['description'],
      id: json['id'],
      productName: json['productName'],
    );
  }
}
