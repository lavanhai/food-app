part of 'user_bloc.dart';

sealed class UserEvent extends Equatable {
  const UserEvent();
}

class SetUSer extends UserEvent{
  final UserModel userModel;
  const SetUSer({required this.userModel});
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class DeleteUSer extends UserEvent{
  @override
  // TODO: implement props
  List<Object?> get props => [];
}
