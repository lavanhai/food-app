part of 'user_bloc.dart';

 class UserState extends Equatable {
  const UserState({this.userModel});

  final UserModel? userModel;

  @override
  // TODO: implement props
  List<Object?> get props => [userModel];
}
