import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:food_app/models/user_model.dart';

part 'user_event.dart';

part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(const UserState()) {
    on<SetUSer>((event, emit) {
      emit(UserState(userModel: event.userModel));
    });
    on<DeleteUSer>((event, emit) {
      emit(const UserState());
    });
  }
}
