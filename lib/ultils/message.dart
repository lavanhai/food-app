import 'package:flutter/material.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class Message {
  static void show(
      {required BuildContext context, String? message, bool? isSuccess}) {
    if (isSuccess == true) {
      showTopSnackBar(
        Overlay.of(context),
        CustomSnackBar.success(
          message: message ?? "",
        ),
      );
    } else {
      showTopSnackBar(
        Overlay.of(context),
        CustomSnackBar.error(
          message: message ?? "",
        ),
      );
    }
  }
}
