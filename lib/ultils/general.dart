import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:food_app/models/user_model.dart';
import 'package:intl/intl.dart';

bool validateEmail(String email) {
  // Biểu thức chính quy để xác thực email
  String emailPattern =
      r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$';
  RegExp regex = RegExp(emailPattern);
  return regex.hasMatch(email);
}

double convertVNDToUSD(int amountInVND, double exchangeRate) {
  // Chuyển đổi VND sang USD
  double amountInUSD = amountInVND / exchangeRate;
  return amountInUSD;
}

int convertCurrencyStringToInt(String currency) {
  // Loại bỏ các ký tự không phải là số hoặc dấu chấm
  String cleanedString = currency.replaceAll(RegExp(r'[^\d]'), '');

  // Chuyển đổi chuỗi thành số nguyên
  int amount = int.parse(cleanedString);

  return amount;
}

String convertGlobalPhone(String phoneNumber) {
  if (!phoneNumber.startsWith('+')) {
    phoneNumber = '+84${phoneNumber.substring(1)}';
  }
  return phoneNumber;
}


String formatDateTime(DateTime dateTime) {
  final DateFormat formatter = DateFormat('dd/MM/yyyy HH:mm');
  return formatter.format(dateTime);
}

String idGenerator() {
  final now = DateTime.now();
  return now.microsecondsSinceEpoch.toString();
}

bool isAdmin(UserModel? userModel) {
  if (userModel?.role == 1) {
    return true;
  }
  return false;
}

void setData(
    {required String tableName,
    required String id,
    required Map<String, dynamic> data,
    VoidCallback? onSuccess,
    Function(Map<String, dynamic>)? onExists,
    Function(String)? onError}) async {
  CollectionReference collection =
      FirebaseFirestore.instance.collection(tableName);
  DocumentSnapshot userDoc = await collection.doc(id).get();

  if (onExists == null ? false : userDoc.exists) {
    Map<String, dynamic> data =
    userDoc.data()! as Map<String, dynamic>;
    onExists.call(data);
  } else {
    collection.doc(id).set(data).then((value) {
      onSuccess?.call();
    }).catchError((error) {
      onError?.call(error.toString());
    });
  }
}

Future<void> deleteData(
    {required String id,
    required String table,
    VoidCallback? onSuccess,
    Function(String)? onError}) {
  CollectionReference collection = FirebaseFirestore.instance.collection(table);
  return collection
      .doc(id)
      .delete()
      .then((value) => onSuccess?.call())
      .catchError((error) => onError?.call(error));
}
