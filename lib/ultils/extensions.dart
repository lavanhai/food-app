import 'package:intl/intl.dart';

extension StringFormat on String {
  String formatAmount() {
    num number = num.parse(this);
    if (number < 1000) {
      return number.toString();
    } else if (number < 1000000) {
      double result = number / 1000;
      double roundedNumber = (result * 10).floor() / 10;
      String formattedNumber =
          roundedNumber.toStringAsFixed(1).replaceAll('.', ',');
      if (formattedNumber.endsWith(',0')) {
        formattedNumber = formattedNumber.replaceAll(',0', '');
      }
      return '${formattedNumber}K';
    } else if (number < 1000000000) {
      double result = number / 1000000;
      double roundedNumber = (result * 10).floor() / 10;
      String formattedNumber =
          roundedNumber.toStringAsFixed(1).replaceAll('.', ',');
      if (formattedNumber.endsWith(',0')) {
        formattedNumber = formattedNumber.replaceAll(',0', '');
      }
      return '${formattedNumber}M';
    } else {
      double result = number / 1000000000;
      double roundedNumber = (result * 10).floor() / 10;
      String formattedNumber =
          roundedNumber.toStringAsFixed(1).replaceAll('.', ',');
      if (formattedNumber.endsWith(',0')) {
        formattedNumber = formattedNumber.replaceAll(',0', '');
      }
      return '${formattedNumber}B';
    }
  }

  String formatPrice() {
    final NumberFormat currencyFormat =
        NumberFormat.currency(locale: 'vi_VN', symbol: '₫');
    num money = num.parse(this);
    return currencyFormat.format(money);
  }

  bool isInteger() {
    // Attempt to parse the string as an integer
    final int? value = int.tryParse(this);
    // Check if the result is not null
    return value != null;
  }

  bool isPhoneNumber() {
    RegExp regExp = RegExp(r'^(?:[+0]9)?[0-9]{10}$');
    if (length == 0) {
      return false;
    } else if (!regExp.hasMatch(this)) {
      return false;
    }
    return true;
  }
}
