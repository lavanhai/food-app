import 'dart:async';

class DebounceTimer {
  Timer? _timer;

  void start(Function() callback, Duration duration) {
    cancel();
    _timer = Timer(duration, callback);
  }

  void cancel() {
    if (_timer != null && _timer!.isActive) {
      _timer!.cancel();
    }
  }
}
