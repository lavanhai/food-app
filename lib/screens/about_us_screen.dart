import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/styles/font_styles.dart';

class AboutUsScreen extends StatelessWidget {
  const AboutUsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("About us"),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                  borderRadius: BorderRadius.circular(16),
                  child: Image.network(
                      "https://picturedensity.com/wp-content/uploads/2023/10/Frist-Class-Fast-Food-5x3-ft-PMC_0128.jpg")),
              const SizedBox(
                height: 16,
              ),
              const Card(
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "McDonald's tại Việt Nam",
                        style: textBold14,
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        "Có mặt tại hơn 118 quốc gia với chuỗi 35,000 nhà hàng tại khắp các châu lục, mỗi ngày, McDonald’s toàn cầu phục vụ hơn 70 triệu người tiêu dùng, không chỉ đảm bảo mang đến cho họ những bữa ăn ngon, an toàn vệ sinh, mà còn làm họ hài lòng với dịch vụ của McDonald’s.",
                        style: textRegular14,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              const Card(
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Tầm nhìn & hoài bão thương hiệu",
                        style: textBold14,
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        """McDonald’s sẽ thiết lập một chuẩn mực mới cho ngành công nghiệp nhà hàng phục vụ thức ăn nhanh tại Việt Nam, mang đến cho khách hàng những trải nghiệm độc nhất chỉ có tại chuỗi nhà hàng của chúng tôi.

Hoài bão của chúng tôi là phục vụ Thức ăn ngon cùng đội ngũ Nhân Viên Chuyên Nghiệp, Thân Thiện và là một Thành Viên Tốt của cộng đồng.
                        """,
                        style: textRegular14,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              const Card(
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Sứ mạng của thương hiệu",
                        style: textBold14,
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        """McDonald’s Việt Nam cam kết áp dụng tiêu chuẩn của McDonald’s toàn cầu, đó là: Quality - Chất lượng, Service - Dịch vụ, Cleanliness - Vệ sinh & Values - Giá trị.

•High quality food: Thực phẩm chất lượng cao
•Superior service: Phục vụ chuyên nghiệp
•Clean and welcoming environment: Môi trường sạch sẽ và thân thiện
•Great value for money: Giá cả hợp lý""",
                        style: textRegular14,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 24,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
