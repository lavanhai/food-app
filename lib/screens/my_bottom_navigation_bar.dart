import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_app/models/user_model.dart';
import 'package:food_app/screens/navigation_screen/cart_screen.dart';
import 'package:food_app/screens/navigation_screen/history_screen.dart';
import 'package:food_app/screens/navigation_screen/home_screen.dart';
import 'package:food_app/screens/navigation_screen/setting_screen.dart';
import 'package:food_app/styles/my_colors.dart';

import '../blocs/user_bloc/user_bloc.dart';
import '../ultils/general.dart';
import 'navigation_screen/voucher_screen.dart';

class MyBottomNavigationBar extends StatefulWidget {
  const MyBottomNavigationBar({super.key});

  @override
  State<MyBottomNavigationBar> createState() => _MyBottomNavigationBarState();
}

class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {
  int _selectedIndex = 0;
  UserModel? _userModel;
  late final List<Widget> _listScreens;
  late final List<BottomNavigationBarItem> _listIcon;

  @override
  void initState() {
    // TODO: implement initState
    _userModel = context.read<UserBloc>().state.userModel;
    if (isAdmin(_userModel)) {
      _listScreens = [
        const HomeScreen(
          isAdmin: true,
        ),
        // const VoucherScreen(
        //   isAdmin: true,
        // ),
        const SettingScreen()
      ];
      _listIcon = [
        const BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        // const BottomNavigationBarItem(
        //   icon: Icon(Icons.card_giftcard),
        //   label: 'Voucher',
        // ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.account_circle_outlined),
          label: 'Menu',
        ),
      ];
    } else {
      _listScreens = [
        HomeScreen(
          isAdmin: false,
          onJumpToVoucherTab: () {
            setState(() {
              _selectedIndex = 2;
            });
          },
        ),
        const CartScreen(),
        const VoucherScreen(
          isAdmin: false,
        ),
        const HistoryScreen(),
        const SettingScreen()
      ];
      _listIcon = [
        const BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.shopping_cart),
          label: 'Cart',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.card_giftcard),
          label: 'Voucher',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.history),
          label: 'History',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.account_circle_outlined),
          label: 'Menu',
        ),
      ];
    }
    super.initState();
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _listScreens.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: _listIcon,
        currentIndex: _selectedIndex,
        selectedItemColor: MyColors.orange,
        unselectedItemColor: MyColors.titleHind,
        onTap: _onItemTapped,
      ),
    );
  }
}
