import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:food_app/models/product_model.dart';
import 'package:food_app/screens/modals/dialog_message.dart';
import 'package:food_app/styles/font_styles.dart';
import 'package:food_app/styles/my_colors.dart';
import 'package:food_app/ultils/extensions.dart';
import 'package:food_app/ultils/general.dart';
import 'package:loader_overlay/loader_overlay.dart';

import '../../models/coupon_model.dart';
import '../../ultils/message.dart';
import '../create_product_screen.dart';
import '../detail_product_screen.dart';
import '../edit_banner_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key, required this.isAdmin, this.onJumpToVoucherTab});

  final bool isAdmin;
  final VoidCallback? onJumpToVoucherTab;

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final Stream<QuerySnapshot> _productsStream =
      FirebaseFirestore.instance.collection('products').snapshots();
  final Stream<QuerySnapshot> _bannersStream =
  FirebaseFirestore.instance.collection('banners').snapshots();
  final List<String> _listImageBanner = [
    "https://img.freepik.com/free-psd/fast-food-concept-banner-template_23-2148777965.jpg",
    "https://img.freepik.com/free-vector/flat-design-fast-food-sale-banner_23-2149165450.jpg",
    "https://i.pinimg.com/736x/10/61/5a/10615aaada889fd2fe2d784afa55d3e9.jpg",
    "https://i.pinimg.com/736x/da/66/24/da66249a283dafab8488b5c3bddf56f1.jpg",
    "https://img.freepik.com/free-vector/flat-design-organic-food-sale-banner-template_23-2149112289.jpg?size=626&ext=jpg"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Hot',
          style: textRegular17,
        ),
        actions: [
          if (widget.isAdmin)
            IconButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const CreateProductScreen(),
                      ));
                },
                icon: const Icon(Icons.add))
        ],
      ),
      body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // CarouselSlider(
                //   options: CarouselOptions(
                //     // height: 200,
                //     height: 150,
                //     autoPlay: true,
                //     autoPlayInterval: const Duration(seconds: 5),
                //   ),
                //   items: _listImageBanner.map((i) {
                //     return Builder(
                //       builder: (BuildContext context) {
                //         return InkWell(
                //           onTap: widget.isAdmin ? () {
                //             Navigator.push(
                //                 context,
                //                 MaterialPageRoute(
                //                   builder: (context) =>
                //                   const EditBannerScreen(),
                //                 ));
                //           } : null,
                //           child: ClipRRect(
                //               borderRadius: BorderRadius.circular(8),
                //               child: Image.network(i)),
                //         );
                //       },
                //     );
                //   }).toList(),
                // ),
                 _CarouselBanner(isAdmin: widget.isAdmin,),
                const SizedBox(
                  height: 18,
                ),
                _CarouselVoucher(
                  onJumpToVoucherTab: widget.onJumpToVoucherTab,
                ),
                const SizedBox(
                  height: 12,
                ),
                const Text(
                  "Products",
                  style: textRegular17,
                ),
                StreamBuilder<QuerySnapshot>(
                    stream: _productsStream,
                    builder: (context, snapshot) {
                      if (snapshot.hasError) {
                        return const Text('Something went wrong');
                      }

                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const Text("Loading");
                      }
                      return GridView(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2, // Number of items per row
                          childAspectRatio: 0.75, // Aspect ratio of each item
                          crossAxisSpacing: 8.0, // Horizontal space between items
                          mainAxisSpacing: 8.0, // Vertical space between items
                        ),
                        children:
                        snapshot.data!.docs.map((DocumentSnapshot document) {
                          Map<String, dynamic> data =
                          document.data()! as Map<String, dynamic>;
                          ProductModel productModel = ProductModel.fromJson(data);
                          return ProductItem(
                            isAdmin: widget.isAdmin,
                            productModel: productModel,
                          );
                        }).toList(),
                      );
                    }
                ),
              ],
            ),
          )),
    );
  }
}

class ProductItem extends StatelessWidget {
  final ProductModel productModel;
  final bool isAdmin;

  const ProductItem(
      {super.key, required this.productModel, required this.isAdmin});

  void _onCLickItem(BuildContext context) {
    if (isAdmin) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CreateProductScreen(
                    productModel: productModel,
                  )));
    } else {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) =>
                DetailProductScreen(productModel: productModel),
          ));
    }
  }

  void _onDeleteProduct(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => DialogMessage(
            onConfirm: () {
              context.loaderOverlay.show();
              deleteData(
                id: productModel.id!,
                table: "products",
                onSuccess: () {
                  context.loaderOverlay.hide();
                  Navigator.pop(context);
                  Message.show(
                      context: context, message: "Delete product success");
                },
                onError: (String message) {
                  context.loaderOverlay.hide();
                  return Message.show(
                      context: context, message: "Error: $message");
                },
              );
            },
            title: "Delete product",
            message: "Are you sure you want to delete this product?"));
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _onCLickItem(context),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image.network(productModel.image ?? "",
                      fit: BoxFit.fill)),
            ),
            const SizedBox(height: 8.0),
            Text(
              productModel.productName ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 4.0),
            Row(
              children: [
                Expanded(
                  child: Text(
                    productModel.price.toString().formatPrice(),
                    style: const TextStyle(fontSize: 16, color: Colors.green),
                  ),
                ),
                if (isAdmin)
                  InkWell(
                    onTap: () => _onDeleteProduct(context),
                    child: const Icon(
                      Icons.delete_forever,
                      color: Colors.red,
                    ),
                  )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _CarouselVoucher extends StatelessWidget {
  const _CarouselVoucher({super.key, this.onJumpToVoucherTab});

  final VoidCallback? onJumpToVoucherTab;

  @override
  Widget build(BuildContext context) {
    final Stream<QuerySnapshot> couponsStream =
        FirebaseFirestore.instance.collection('coupons').snapshots();
    return StreamBuilder<QuerySnapshot>(
      stream: couponsStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return const Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Text("Loading");
        }

        final filteredDocs =
            snapshot.data!.docs.where((DocumentSnapshot document) {
          Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
          return data["quantity"] != "0";
        }).toList();

        return SizedBox(
          height: 50,
          child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            children: filteredDocs.map((DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data()! as Map<String, dynamic>;
              CouponModel couponModel = CouponModel.fromJson(data);
              return GestureDetector(
                onTap: onJumpToVoucherTab,
                child: Container(
                  height: 50,
                  margin: const EdgeInsets.only(right: 16),
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  decoration: BoxDecoration(
                      border: Border.all(color: MyColors.disable),
                      borderRadius: BorderRadius.circular(16)),
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/icons/gift-card.png",
                        height: 36,
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      Text(
                        "-${couponModel.discount!.formatAmount()}",
                        style: textBold14.copyWith(color: Colors.red),
                      )
                    ],
                  ),
                ),
              );
            }).toList(),
          ),
        );
      },
    );
  }
}

class _CarouselBanner extends StatelessWidget {
  const _CarouselBanner({super.key, required this.isAdmin});
  final bool isAdmin;

  @override
  Widget build(BuildContext context) {
    final Stream<QuerySnapshot> bannersStream =
    FirebaseFirestore.instance.collection('banners').snapshots();
    return StreamBuilder<QuerySnapshot>(
      stream: bannersStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return const Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Text("Loading");
        }

        final filteredDocs =
        snapshot.data!.docs.map((DocumentSnapshot document) {
          Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
          return data;
        });
        return
        CarouselSlider(
          options: CarouselOptions(
            // height: 200,
            height: 150,
            autoPlay: true,
            autoPlayInterval: const Duration(seconds: 5),
          ),
          items: filteredDocs.map((item) {
            return Builder(
              builder: (BuildContext context) {
                return InkWell(
                  onTap: isAdmin ? () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                           EditBannerScreen(itemBanner: item),
                        ));
                  } : null,
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Image.network(item["image"])),
                );
              },
            );
          }).toList(),
        );

        // return InkWell(
        //   onTap: isAdmin ? () {
        //     Navigator.push(
        //         context,
        //         MaterialPageRoute(
        //           builder: (context) =>
        //           const EditBannerScreen(),
        //         ));
        //   } : null,
        //   child: ClipRRect(
        //       borderRadius: BorderRadius.circular(8),
        //       child: Image.network("")),
        // );
      },
    );
  }
}

