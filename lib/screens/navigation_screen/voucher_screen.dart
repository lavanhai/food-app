import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_app/blocs/user_bloc/user_bloc.dart';
import 'package:food_app/models/coupon_model.dart';
import 'package:food_app/models/user_model.dart';
import 'package:food_app/ultils/extensions.dart';
import 'package:food_app/ultils/general.dart';
import 'package:food_app/ultils/message.dart';
import 'package:food_app/widgets/my_text_field.dart';
import 'package:loader_overlay/loader_overlay.dart';

import '../../widgets/coupon_card.dart';
import '../modals/dialog_message.dart';

class VoucherScreen extends StatelessWidget {
  const VoucherScreen({super.key, required this.isAdmin});

  final bool isAdmin;

  void _onGetVoucher(
      {required CouponModel couponModel,
      required BuildContext context,
      required String phone}) {
    int quantity = int.parse(couponModel.quantity!) - 1;
    Map<String, dynamic> usersCoupon = couponModel.users ?? {};
    usersCoupon[phone] = true;
    setData(
      id: couponModel.id!,
      tableName: "coupons",
      data: couponModel
          .copyWith(quantity: quantity.toString(), users: usersCoupon)
          .toJson(),
    );
  }

  void _onDeleteVoucher(
      {required CouponModel couponModel, required BuildContext context}) {
    showDialog(
        context: context,
        builder: (context) => DialogMessage(
            onConfirm: () {
              context.loaderOverlay.show();
              deleteData(
                id: couponModel.id!,
                table: "coupons",
                onSuccess: () {
                  context.loaderOverlay.hide();
                  Navigator.pop(context);
                  return Message.show(
                      context: context,
                      message: "Delete coupon success",
                      isSuccess: true);
                },
                onError: (String message) {
                  context.loaderOverlay.hide();
                  return Message.show(
                      context: context, message: "Error: $message");
                },
              );
            },
            title: "Delete coupon",
            message: "Are you sure you want to delete this coupon?"));
  }

  @override
  Widget build(BuildContext context) {
    UserModel userModel = context.read<UserBloc>().state.userModel!;
    final Stream<QuerySnapshot> couponsStream =
        FirebaseFirestore.instance.collection('coupons').snapshots();

    return Scaffold(
        appBar: AppBar(
          title: const Text("Shopping voucher"),
          actions: [
            if (isAdmin)
              IconButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) => const _DialogCoupon(),
                    );
                  },
                  icon: const Icon(Icons.add))
          ],
        ),
        body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: StreamBuilder<QuerySnapshot>(
              stream: couponsStream,
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError) {
                  return const Center(child: Text('Something went wrong'));
                }

                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(child: Text("Loading"));
                }

                if (snapshot.data == null) {
                  return const Center(child: Text("Voucher is empty"));
                }

                final filteredDocs =
                    snapshot.data!.docs.where((DocumentSnapshot document) {
                  Map<String, dynamic> data =
                      document.data()! as Map<String, dynamic>;
                  Map<String, dynamic>? uVoucher = data["users"];
                  if (isAdmin) {
                    return true;
                  }
                  return uVoucher == null ||
                      uVoucher.isEmpty ||
                      data["users"]?[userModel.phone] != false;
                }).toList();

                return ListView(
                  children: filteredDocs.map((DocumentSnapshot document) {
                    Map<String, dynamic> data =
                        document.data()! as Map<String, dynamic>;
                    CouponModel couponModel = CouponModel.fromJson(data);
                    bool? isGetVoucher = couponModel.users?[userModel.phone];
                    return CouponCard(
                        isAdmin: isAdmin,
                        disable: isGetVoucher != null,
                        buttonName: isGetVoucher == true ? "Saved" : null,
                        onGetVoucher: () => _onGetVoucher(
                            context: context,
                            couponModel: couponModel,
                            phone: userModel.phone!),
                        onDeleteVoucher: () => _onDeleteVoucher(
                            context: context, couponModel: couponModel),
                        discount: couponModel.discount!.formatAmount(),
                        quantity: couponModel.quantity!);
                  }).toList(),
                );
              },
            )));
  }
}

class _DialogCoupon extends StatefulWidget {
  const _DialogCoupon({super.key});

  @override
  State<_DialogCoupon> createState() => _DialogCouponState();
}

class _DialogCouponState extends State<_DialogCoupon> {
  late final TextEditingController _textEditingControllerDiscount;
  late final TextEditingController _textEditingControllerQuantity;

  @override
  void initState() {
    // TODO: implement initState
    _textEditingControllerDiscount = TextEditingController();
    _textEditingControllerQuantity = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _textEditingControllerDiscount.dispose();
    _textEditingControllerQuantity.dispose();
    super.dispose();
  }

  void _onSave() {
    String discount = _textEditingControllerDiscount.text.trim();
    String quantity = _textEditingControllerQuantity.text.trim();
    if (discount.isEmpty || quantity.isEmpty) {
      return Message.show(context: context, message: "Data can not be blank");
    }
    if (!quantity.isInteger()) {
      return Message.show(
          context: context, message: "The quantity is in wrong format");
    }
    String uuid = idGenerator();
    context.loaderOverlay.show();
    setData(
      tableName: "coupons",
      id: uuid,
      data: CouponModel(id: uuid, discount: discount, quantity: quantity)
          .toJson(),
      onSuccess: () {
        context.loaderOverlay.hide();
        Navigator.pop(context);
        Message.show(
            context: context,
            message: "Create coupon success",
            isSuccess: true);
      },
      onError: (String message) {
        context.loaderOverlay.hide();
        return Message.show(context: context, message: "Error: $message");
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Create coupon'),
      surfaceTintColor: Colors.white,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          MyTextField(
            title: "Discount",
            keyboardType: TextInputType.phone,
            hind: "Enter the discount amount",
            controller: _textEditingControllerDiscount,
          ),
          const SizedBox(
            height: 16,
          ),
          MyTextField(
            title: "Quantity",
            keyboardType: TextInputType.phone,
            hind: "Enter the quantity",
            controller: _textEditingControllerQuantity,
          ),
        ],
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: const Text('Cancel'),
        ),
        TextButton(
          onPressed: _onSave,
          child: const Text('OK'),
        ),
      ],
    );
  }
}
