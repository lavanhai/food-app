import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_app/blocs/user_bloc/user_bloc.dart';
import 'package:food_app/models/user_model.dart';
import 'package:food_app/screens/auth/auth_screen.dart';
import 'package:food_app/screens/manager_user_screen.dart';
import 'package:food_app/styles/font_styles.dart';
import 'package:food_app/ultils/global.dart';

import '../../ultils/general.dart';
import '../about_us_screen.dart';
import '../edit_profile_screen.dart';

class SettingScreen extends StatelessWidget {
  const SettingScreen({super.key});

  void _onLogout(BuildContext context) {
    BlocProvider.of<UserBloc>(context).add(DeleteUSer());
    removeToken();
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => const AuthScreen(),
        ));
  }

  void _onNavigateAboutUs(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => const AboutUsScreen(),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Profile"),
        actions: [
            IconButton(
                onPressed: () {
                  UserModel userModel = context.read<UserBloc>().state.userModel!;
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          if(isAdmin(userModel)){
                            return const ManagerUserScreen();
                          }else{
                            return EditProfileScreen(
                              userModel: userModel,
                            );
                          }
                        },
                      ));
                },
                icon: const Icon(Icons.settings))
        ],
      ),
      body: BlocBuilder<UserBloc, UserState>(
        builder: (context, state) {
          return SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListTile(
                    title: const Text("Full name"),
                    subtitle: Text(state.userModel?.fullName ?? "User"),
                    leading: const Icon(Icons.person),
                  ),
                  ListTile(
                    title: const Text("Phone number"),
                    subtitle: Text(state.userModel?.phone ?? "0987654321"),
                    leading: const Icon(Icons.phone_enabled_sharp),
                  ),
                  ListTile(
                    title: const Text("Email"),
                    subtitle: Text(state.userModel?.email ?? "example@gmail.com"),
                    leading: const Icon(Icons.mail),
                  ),
                  ListTile(
                    title: const Text("Address"),
                    subtitle: Text(state.userModel?.address ?? "TP HCM"),
                    leading: const Icon(Icons.location_on),
                  ),
                  ListTile(
                    onTap: () => _onNavigateAboutUs(context),
                    title: const Text("About us"),
                    leading: const Icon(Icons.info),
                    trailing: const Icon(Icons.arrow_forward_ios_rounded),
                  ),
                  ListTile(
                    onTap: () => _onLogout(context),
                    title: const Text(
                      "Logout",
                      style: TextStyle(color: Colors.red),
                    ),
                    leading: const Icon(
                      Icons.logout,
                      color: Colors.red,
                    ),
                    trailing: const Icon(Icons.arrow_forward_ios_rounded,
                        color: Colors.red),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
