import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_paypal/flutter_paypal.dart';
import 'package:food_app/blocs/user_bloc/user_bloc.dart';
import 'package:food_app/models/user_model.dart';
import 'package:food_app/screens/modals/modal_voucher.dart';
import 'package:food_app/styles/font_styles.dart';
import 'package:food_app/styles/my_colors.dart';
import 'package:food_app/ultils/extensions.dart';
import 'package:food_app/ultils/general.dart';
import 'package:food_app/widgets/counter_widget.dart';
import 'package:food_app/widgets/my_button_fill.dart';
import 'package:loader_overlay/loader_overlay.dart';

import '../../models/cart_model.dart';
import '../../models/coupon_model.dart';
import '../../ultils/debounce.dart';
import '../../ultils/message.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  late UserModel _userModel;
  late final Stream<DocumentSnapshot> _cartsStream;
  CouponModel? _voucherSelected;

  @override
  void initState() {
    // TODO: implement initState
    _userModel = context.read<UserBloc>().state.userModel!;
    _cartsStream = FirebaseFirestore.instance
        .collection('carts')
        .doc(_userModel.id)
        .snapshots();
    super.initState();
  }

  double _calculateTotal(List<CartModel> cartList) {
    double total = 0.0;
    for (CartModel cartItem in cartList) {
      total +=
          int.parse(cartItem.product!['price']) * int.parse(cartItem.quantity!);
    }
    return total;
  }

  void _onSelectVoucher(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (context) => ModalVoucher(
        voucherSelected: _voucherSelected,
        onSelected: (CouponModel? couponSelected) {
          setState(() {
            _voucherSelected = couponSelected;
          });
        },
      ),
    );
  }

  String _getTotalToPay(List<CartModel> cartList) {
    double total = _calculateTotal(cartList) -
        (double.parse(_voucherSelected?.discount.toString() ?? "0"));
    if(total < 0){
      return 0.toString().formatPrice();
    }
    return total.toString().formatPrice();
  }

  void _onPaymentWithPaypal(BuildContext context, List<CartModel> cartList) {
    String strTotal = _getTotalToPay(cartList);
    num totalInt = convertCurrencyStringToInt(strTotal);
    num totalDollar = convertVNDToUSD(totalInt.toInt(),23000);
    // print(totalDollar.toStringAsFixed(2));
    // return;
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) => UsePaypal(
            sandboxMode: true,
            clientId:
                "AW1TdvpSGbIM5iP4HJNI5TyTmwpY9Gv9dYw8_8yW5lYIbCqf326vrkrp0ce9TAqjEGMHiV3OqJM_aRT0",
            secretKey:
                "EHHtTDjnmTZATYBPiGzZC_AZUfMpMAzj2VZUeqlFUrRJA_C0pQNCxDccB5qoRQSEdcOnnKQhycuOWdP9",
            returnURL: "https://samplesite.com/return",
            cancelURL: "https://samplesite.com/cancel",
            transactions: const [
              {
                "amount": {
                  "total": '10.12',
                  "currency": "USD",
                  "details": {
                    "subtotal": '10.12',
                    "shipping": '0',
                    "shipping_discount": 0
                  }
                },
                "description": "The payment transaction description.",
                // "payment_options": {
                //   "allowed_payment_method":
                //       "INSTANT_FUNDING_SOURCE"
                // },
                "item_list": {
                  "items": [
                    {
                      "name": "A demo product",
                      "quantity": 1,
                      "price": '10.12',
                      "currency": "USD"
                    }
                  ],

                  // shipping address is not required though
                  "shipping_address": {
                    "recipient_name": "Jane Foster",
                    "line1": "Travis County",
                    "line2": "",
                    "city": "Austin",
                    "country_code": "US",
                    "postal_code": "73301",
                    "phone": "+00000000",
                    "state": "Texas"
                  },
                }
              }
            ],
            note: "Contact us for any questions on your order.",
            onSuccess: (Map params) async {
              print("onSuccess: $params");
            },
            onError: (error) {
              print("onError: $error");
            },
            onCancel: (params) {
              print('cancelled: $params');
            }),
      ),
    );
  }

  void _onPayment(
      {required BuildContext context,
      required List<CartModel> cartList}) async {
    String strTotal = _getTotalToPay(cartList);
    String uuid = idGenerator();
    UserModel userModel = context.read<UserBloc>().state.userModel!;
    context.loaderOverlay.show();
    DocumentReference orderRef =
        FirebaseFirestore.instance.collection('orders').doc(userModel.id);
    DocumentSnapshot cartSnapshot = await orderRef.get();
    Map<String, dynamic> usersCoupon = _voucherSelected?.users ?? {};
    if (cartSnapshot.exists) {
      orderRef.update({
        uuid: {
          "id": uuid,
          "paymentTime": DateTime.now().toIso8601String(),
          "totalPayment": strTotal,
          "products": cartList.map((cart) => cart.toJson()).toList(),
          "discount": _voucherSelected?.discount ?? "0"
        }
      }).then((value) {
        context.loaderOverlay.hide();
        deleteData(id: userModel.id!, table: "carts");
        if (_voucherSelected != null) {
          usersCoupon[userModel.id!] = false;
          setData(
            id: _voucherSelected!.id!,
            tableName: "coupons",
            data: _voucherSelected!.copyWith(users: usersCoupon).toJson(),
          );
        }
        Message.show(context: context, message: 'Payment success',isSuccess: true);
      }).catchError((error) {
        context.loaderOverlay.hide();
        Message.show(context: context, message: 'Error: $error');
      });
    } else {
      setData(
        tableName: "orders",
        id: userModel.id!,
        data: {
          uuid: {
            "id": uuid,
            "paymentTime": DateTime.now().toIso8601String(),
            "totalPayment": strTotal,
            "products": cartList.map((cart) => cart.toJson()).toList(),
            "discount": _voucherSelected?.discount ?? "0"
          }
        },
        onSuccess: () {
          context.loaderOverlay.hide();
          deleteData(id: userModel.id!, table: "carts");
          if (_voucherSelected != null) {
            usersCoupon[userModel.id!] = false;
            setData(
              id: _voucherSelected!.id!,
              tableName: "coupons",
              data: _voucherSelected!.copyWith(users: usersCoupon).toJson(),
            );
          }
          Message.show(context: context, message: 'Payment success',isSuccess: true);
        },
        onError: (String message) {
          context.loaderOverlay.hide();
          Message.show(context: context, message: 'Error: $message');
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Shopping cart"),
        ),
        body: StreamBuilder<DocumentSnapshot>(
          stream: _cartsStream,
          builder:
              (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            }

            if (snapshot.hasError) {
              return const Center(child: Text('Something went wrong'));
            }

            if (!snapshot.hasData || !snapshot.data!.exists) {
              return const Center(child: Text('Cart is empty'));
            }

            Map<String, dynamic> data =
                snapshot.data!.data() as Map<String, dynamic>;
            List<CartModel> cartList = data.entries.map((entry) {
              return CartModel(
                  id: entry.value['id'],
                  quantity: entry.value['quantity'],
                  product: entry.value['product']);
            }).toList();
            return Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: cartList.length,
                      itemBuilder: (context, index) {
                        CartModel cartModel = cartList[index];
                        return _ItemCart(
                          cartModel: cartModel,
                        );
                      },
                    ),
                  ),
                ),
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ListTile(
                          onTap: () => _onSelectVoucher(context),
                          contentPadding: EdgeInsets.zero,
                          visualDensity: const VisualDensity(vertical: -4),
                          horizontalTitleGap: 8,
                          title: Text(
                            _voucherSelected == null
                                ? "Select voucher"
                                : "- ${_voucherSelected?.discount.toString().formatAmount()}",
                            style: textBold14.copyWith(color: MyColors.orange),
                          ),
                          leading: const Icon(
                            Icons.discount,
                            color: MyColors.orange,
                          ),
                          trailing: const Icon(Icons.keyboard_arrow_right,
                              color: MyColors.orange),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text("Address"),
                            const SizedBox(width: 16),
                            Expanded(
                                child: Text(_userModel.address!,
                                  textAlign: TextAlign.right,
                                  style: textBold14,
                                )),
                          ],
                        ),
                        Row(
                          children: [
                            const Text("Total amount"),
                            const SizedBox(width: 16),
                            Expanded(
                                child: Text(
                              _calculateTotal(cartList)
                                  .toString()
                                  .formatPrice(),
                              textAlign: TextAlign.right,
                              style: textBold14,
                            )),
                          ],
                        ),
                        Row(
                          children: [
                            const Text("Discount"),
                            const SizedBox(width: 16),
                            Expanded(
                                child: Text(
                              "- ${(_voucherSelected?.discount ?? 0).toString().formatPrice()}",
                              textAlign: TextAlign.right,
                              style:
                                  textBold14.copyWith(color: MyColors.orange),
                            )),
                          ],
                        ),
                        Row(
                          children: [
                            const Text("Total to pay"),
                            const SizedBox(width: 16),
                            Expanded(
                                child: Text(
                              _getTotalToPay(cartList),
                              textAlign: TextAlign.right,
                              style: textBold14,
                            )),
                          ],
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: MyButtonFill(
                                title: "Cash",
                                onPressed: () =>
                                    _onPayment(context: context, cartList: cartList),
                              ),
                            ),
                            const SizedBox(width: 16,),
                            Expanded(
                              child: MyButtonFill(
                                title: "Paypal",
                                onPressed: () =>
                                    _onPaymentWithPaypal(context, cartList)
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            );
          },
        ));
  }
}

class _ItemCart extends StatefulWidget {
  const _ItemCart({super.key, required this.cartModel});

  final CartModel cartModel;

  @override
  State<_ItemCart> createState() => _ItemCartState();
}

class _ItemCartState extends State<_ItemCart> {
  final DebounceTimer _debounceTimer = DebounceTimer();
  late UserModel _userModel;
  late DocumentReference _cartRef;

  late int _quantity;

  @override
  void initState() {
    // TODO: implement initState
    _quantity = int.parse(widget.cartModel.quantity!);
    _userModel = context.read<UserBloc>().state.userModel!;
    _cartRef =
        FirebaseFirestore.instance.collection('carts').doc(_userModel.id);
    super.initState();
  }

  void _onChangeQuantity(int quantity) {
    _debounceTimer.start(() {
      _quantity = quantity;
      _updateQuantity();
    }, const Duration(milliseconds: 700));
  }

  Future<void> _removeItemFromCart(BuildContext context) async {
    DocumentReference cartDoc =
        FirebaseFirestore.instance.collection('carts').doc(_userModel.id);
    // Update the cart by removing the specific item
    await cartDoc.update({
      widget.cartModel.id!: FieldValue.delete(),
    }).then((_) {
      Message.show(context: context, message: "Item removed successfully",isSuccess: true);
    }).catchError((error) {
      Message.show(context: context, message: "Failed to remove item: $error");
    });
  }

  void _updateQuantity() async {
    DocumentSnapshot cartSnapshot = await _cartRef.get();
    Map<String, dynamic> cartData = cartSnapshot.data() as Map<String, dynamic>;
    Map<String, dynamic> userCart = cartData[widget.cartModel.product!["id"]];
    userCart['quantity'] = _quantity.toString();
    await _cartRef.update({
      widget.cartModel.product!["id"]: userCart,
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16),
      child: Card(
        surfaceTintColor: Colors.white,
        elevation: 0,
        child: Row(
          children: [
            SizedBox(
              width: 120,
              height: 120,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image.network(
                    widget.cartModel.product!["image"],
                    fit: BoxFit.cover,
                  )),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.cartModel.product!["productName"] ?? "",
                    style: textBold18.copyWith(height: 1),
                    maxLines: 2,
                    textAlign: TextAlign.start,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  CounterWidget(
                    onChange: _onChangeQuantity,
                    value: _quantity,
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    widget.cartModel.product!["price"].toString().formatPrice(),
                    style: textBold14,
                  ),
                ],
              ),
            ),
            IconButton(
                onPressed: () => _removeItemFromCart(context),
                icon: const Icon(
                  Icons.delete_forever,
                  color: Colors.red,
                ))
          ],
        ),
      ),
    );
  }
}
