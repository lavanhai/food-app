import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_app/blocs/user_bloc/user_bloc.dart';
import 'package:food_app/models/user_model.dart';
import 'package:food_app/styles/my_colors.dart';
import 'package:food_app/ultils/extensions.dart';

import '../../models/order_model.dart';
import '../../styles/font_styles.dart';
import '../../ultils/general.dart';
import '../detail_history_screen.dart';

class HistoryScreen extends StatelessWidget {
  const HistoryScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final UserModel userModel = context.read<UserBloc>().state.userModel!;
    final Stream<DocumentSnapshot> ordersStream = FirebaseFirestore.instance
        .collection('orders')
        .doc(userModel.id)
        .snapshots();
    return Scaffold(
        appBar: AppBar(
          title: const Text("Order history"),
        ),
        body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: StreamBuilder<DocumentSnapshot>(
              stream: ordersStream,
              builder: (BuildContext context,
                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                if (snapshot.hasError) {
                  return const Center(child: Text('Something went wrong'));
                }

                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(child: Text("Loading"));
                }
                if( snapshot.data?.data() == null){
                  return const Center(child: Text("Order history is empty"));
                }

                Map<String, dynamic> data =
                    snapshot.data?.data() as Map<String, dynamic>;
                List<OrderModel> orderList = data.entries.map((entry) {
                  // Access the value property of MapEntry
                  Map<String, dynamic> entryValue =
                      entry.value as Map<String, dynamic>;
                  return OrderModel.fromJson(entryValue);
                }).toList();

                return ListView.builder(
                  itemCount: orderList.length,
                  itemBuilder: (context, index) {
                    return _ItemOrder(
                      orderModel: orderList[index],
                    );
                  },
                );
              },
            )));
  }
}

class _ItemOrder extends StatelessWidget {
  const _ItemOrder({super.key, required this.orderModel});

  final OrderModel orderModel;

  void _onDetail(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DetailHistoryScreen(orderModel: orderModel,),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          formatDateTime(orderModel.paymentTime),
          style: textRegular14.copyWith(color: MyColors.titleHind),
        ),
        Row(
          children: [
            Text(
              orderModel.id,
              style: textBold14,
            ),
            const Spacer(),
            TextButton(
                onPressed: () => _onDetail(context),
                child: const Text(
                  "Details",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      color: MyColors.titleHind),
                ))
          ],
        ),
        ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: orderModel.products.length,
          itemBuilder: (context, index) {
          return _ItemProduct(productOrder: orderModel.products[index],);
        },),
        const Divider(
          thickness: 1,
        )
      ],
    );
  }
}

class _ItemProduct extends StatelessWidget {
  const _ItemProduct({super.key, required this.productOrder});
  final ProductOrder productOrder;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16),
      child: Row(
        children: [
          SizedBox(
            width: 100,
            height: 80,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Image.network(
                  productOrder.product.image,
                  fit: BoxFit.cover,
                )),
          ),
          const SizedBox(
            width: 16,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  productOrder.product.productName,
                  style: textBold18.copyWith(height: 1),
                  maxLines: 2,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  "Quantity: ${ productOrder.quantity}",
                  style: textBold14,
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  productOrder.product.price.formatPrice(),
                  style: textBold14.copyWith(color: MyColors.orange),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
