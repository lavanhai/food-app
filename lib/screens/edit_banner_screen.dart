import 'package:flutter/material.dart';

import '../ultils/general.dart';
import '../ultils/message.dart';
import '../widgets/my_button_fill.dart';
import '../widgets/my_text_field.dart';

class EditBannerScreen extends StatefulWidget {
  const EditBannerScreen({super.key, required this.itemBanner});

  final Map<String, dynamic> itemBanner;

  @override
  State<EditBannerScreen> createState() => _EditBannerScreenState();
}

class _EditBannerScreenState extends State<EditBannerScreen> {
  late final TextEditingController _textEditingControllerBanner;

  @override
  void initState() {
    // TODO: implement initState
    _textEditingControllerBanner =
        TextEditingController(text: widget.itemBanner["image"]);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _textEditingControllerBanner.dispose();
    super.dispose();
  }

  void _onSave() {
    String banner = _textEditingControllerBanner.text.trim();
    if (banner.isEmpty) {
      return Message.show(context: context, message: 'Data can not be blank');
    }
    setData(
      id: widget.itemBanner["id"],
      tableName: "banners",
      data: {
        "id": widget.itemBanner["id"],
        "image": banner,
      },
      onSuccess: () {
        Navigator.pop(context);
        return Message.show(
            context: context, message: 'Update successful', isSuccess: true);
      },
      onError: (String message) {
        return Message.show(context: context, message: 'Error: $message');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit banner"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              MyTextField(
                title: "Banner image",
                hind: "Enter your banner image link",
                keyboardType: TextInputType.url,
                maxLine: 3,
                controller: _textEditingControllerBanner,
              ),
              const SizedBox(
                height: 24,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 48),
                child: MyButtonFill(
                  title: "Save",
                  onPressed: _onSave,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
