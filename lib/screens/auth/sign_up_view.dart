import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_app/blocs/user_bloc/user_bloc.dart';
import 'package:food_app/models/user_model.dart';
import 'package:food_app/services/api_service.dart';
import 'package:food_app/services/link_api.dart';
import 'package:food_app/styles/font_styles.dart';
import 'package:food_app/styles/my_colors.dart';
import 'package:food_app/ultils/extensions.dart';
import 'package:food_app/ultils/general.dart';
import 'package:food_app/ultils/message.dart';
import 'package:loader_overlay/loader_overlay.dart';

import '../../models/response_model.dart';
import '../../widgets/my_button_fill.dart';
import '../../widgets/my_text_field.dart';
import '../my_bottom_navigation_bar.dart';

class SignUpView extends StatefulWidget {
  const SignUpView({super.key, required this.onSuccess});
  final VoidCallback onSuccess;

  @override
  State<SignUpView> createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  late final TextEditingController _textEditingControllerPhone;
  late final TextEditingController _textEditingControllerFullName;
  late final TextEditingController _textEditingControllerPassword;
  late final TextEditingController _textEditingControllerEmail;
  late final TextEditingController _textEditingControllerAddress;

  @override
  void initState() {
    // TODO: implement initState
    _textEditingControllerPhone = TextEditingController();
    _textEditingControllerPassword = TextEditingController();
    _textEditingControllerFullName = TextEditingController();
    _textEditingControllerEmail = TextEditingController();
    _textEditingControllerAddress = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _textEditingControllerPhone.dispose();
    _textEditingControllerPassword.dispose();
    _textEditingControllerFullName.dispose();
    _textEditingControllerEmail.dispose();
    _textEditingControllerAddress.dispose();
    super.dispose();
  }

  void _onSignUp() async {
    String phone = _textEditingControllerPhone.text.trim();
    String fullName = _textEditingControllerFullName.text.trim();
    String password = _textEditingControllerPassword.text.trim();
    String email = _textEditingControllerEmail.text.trim();
    String address = _textEditingControllerAddress.text.trim();

    if (phone.isEmpty ||
        password.isEmpty ||
        fullName.isEmpty ||
        email.isEmpty ||
        address.isEmpty) {
      return Message.show(context: context, message: 'Data can not be blank');
    }

    // if (!phone.isPhoneNumber()) {
    //   return Message.show(
    //       context: context,
    //       message: 'The phone number is not in the correct format');
    // }
    //
    // if (password.length < 6) {
    //   return Message.show(
    //       context: context, message: 'Password minimum 6 characters');
    // }
    //
    // if (!validateEmail(email)) {
    //   return Message.show(context: context, message: 'Email invalidate');
    // }

    Map<String, dynamic> body = {
      "name": fullName,
      "email": email,
      "password": password,
      "phone_number": phone,
      "address": address,
      "telegram": "user_telegram"
    };

    ResponseModel? responseModel = await ApiService.postData(
        "${LinkApi.domain}/v1/api/users/sign-up", body, true);
    if (responseModel?.code == 200) {
      context.loaderOverlay.hide();
      Message.show(
          context: context, message: responseModel?.message, isSuccess: true);
      widget.onSuccess();
    } else {
      Message.show(context: context, message: responseModel?.message);
    }

    // CollectionReference users = FirebaseFirestore.instance.collection('users');
    // context.loaderOverlay.show();
    // // Kiểm tra xem số điện thoại đã tồn tại hay chưa
    // DocumentSnapshot userDoc = await users.doc(phone).get();
    //
    // if (userDoc.exists) {
    //   // Số điện thoại đã tồn tại, hiển thị thông báo lỗi
    //   context.loaderOverlay.hide();
    //   Message.show(
    //       context: context, message: 'Phone number has been registered');
    // } else {
    //   // Số điện thoại chưa tồn tại, thực hiện đăng ký
    //   users.doc(phone).set({
    //     'phone': phone,
    //     'password': password,
    //     'fullName': fullName,
    //     'id': phone,
    //     'email': email,
    //     'address': address,
    //     'role': 0
    //   }).then((value) {
    //     context.loaderOverlay.hide();
    //     BlocProvider.of<UserBloc>(context).add(SetUSer(
    //         userModel: UserModel(
    //             fullName: fullName,
    //             password: password,
    //             phone: phone,
    //             role: 0)));
    //     Message.show(
    //         context: context, message: 'Register successful', isSuccess: true);
    //     Navigator.pushReplacement(
    //         context,
    //         MaterialPageRoute(
    //           builder: (context) => const MyBottomNavigationBar(),
    //         ));
    //   }).catchError((error) {
    //     context.loaderOverlay.hide();
    //     ScaffoldMessenger.of(context).showSnackBar(
    //         const SnackBar(content: Text('Failed to create account')));
    //   });
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Welcome!",
              style: textBold36,
            ),
            Text("Sign up to your account.",
                style: textRegular17.copyWith(color: MyColors.titleHind)),
            const SizedBox(
              height: 36,
            ),
            MyTextField(
              title: "Phone number",
              hind: "Enter your phone number",
              keyboardType: TextInputType.phone,
              controller: _textEditingControllerPhone,
            ),
            const SizedBox(
              height: 24,
            ),
            MyTextField(
              title: "Password",
              hind: "Enter your password",
              isPassword: true,
              keyboardType: TextInputType.visiblePassword,
              controller: _textEditingControllerPassword,
            ),
            const SizedBox(
              height: 24,
            ),
            MyTextField(
              title: "Full Name",
              hind: "Enter your full name",
              controller: _textEditingControllerFullName,
            ),
            const SizedBox(
              height: 24,
            ),
            MyTextField(
              title: "Email",
              hind: "Enter your email",
              controller: _textEditingControllerEmail,
            ),
            const SizedBox(
              height: 24,
            ),
            MyTextField(
              title: "Address",
              hind: "Enter your address",
              controller: _textEditingControllerAddress,
            ),
            const SizedBox(
              height: 24,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 48),
              child: MyButtonFill(
                title: "Sign Up",
                onPressed: _onSignUp,
              ),
            )
          ],
        ),
      ),
    );
  }
}
