import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_app/blocs/user_bloc/user_bloc.dart';
import 'package:food_app/models/user_model.dart';
import 'package:food_app/screens/modals/my_dialog.dart';
import 'package:food_app/screens/my_bottom_navigation_bar.dart';
import 'package:food_app/services/api_service.dart';
import 'package:food_app/services/link_api.dart';
import 'package:food_app/styles/font_styles.dart';
import 'package:food_app/styles/my_colors.dart';
import 'package:food_app/ultils/extensions.dart';
import 'package:food_app/ultils/general.dart';
import 'package:food_app/ultils/global.dart';
import 'package:food_app/ultils/message.dart';
import 'package:food_app/widgets/my_otp_input.dart';
import 'package:loader_overlay/loader_overlay.dart';

import '../../models/response_model.dart';
import '../../widgets/my_button_fill.dart';
import '../../widgets/my_text_field.dart';

class LoginView extends StatefulWidget {
  const LoginView({super.key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  late final TextEditingController _textEditingControllerEmail;

  // late final TextEditingController _textEditingControllerPhone;
  late final TextEditingController _textEditingControllerPhoneForgotPass;
  late final TextEditingController _textEditingControllerNewPassword;
  late final TextEditingController _textEditingControllerPassword;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String? _verificationId;
  String? _otpCode;

  @override
  void initState() {
    // TODO: implement initState
    _textEditingControllerEmail = TextEditingController();
    // _textEditingControllerPhone = TextEditingController();
    _textEditingControllerPassword = TextEditingController();
    _textEditingControllerPhoneForgotPass = TextEditingController();
    _textEditingControllerNewPassword = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _textEditingControllerEmail.dispose();
    // _textEditingControllerPhone.dispose();
    _textEditingControllerPassword.dispose();
    _textEditingControllerPhoneForgotPass.dispose();
    _textEditingControllerNewPassword.dispose();
    super.dispose();
  }

  void _onLogin() async {
    String email = _textEditingControllerEmail.text.trim();
    // String phone = _textEditingControllerPhone.text.trim();
    String password = _textEditingControllerPassword.text.trim();
    if (email.isEmpty || password.isEmpty) {
      return Message.show(context: context, message: 'Data can not be blank');
    }
    context.loaderOverlay.show();
    ResponseModel? responseModel = await ApiService.postData(
        "${LinkApi.domain}/v1/api/users/sign_in",
        {
          "email": email,
          "password": password,
          // "phone_number":phone,
        },
        true);
    context.loaderOverlay.hide();
    if (responseModel?.code == 200) {
      Message.show(
          context: context, message: responseModel?.message, isSuccess: true);
      UserModel userModel = UserModel.fromJson(responseModel?.data);
      setToken(userModel.token);
      BlocProvider.of<UserBloc>(context).add(SetUSer(userModel: userModel));
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => const MyBottomNavigationBar(),
          ));
    } else {
      Message.show(context: context, message: responseModel?.message);
    }

    // CollectionReference users = FirebaseFirestore.instance.collection('users');
    // context.loaderOverlay.show();
    // QuerySnapshot querySnapshot = await users
    //     .where('phone', isEqualTo: phone)
    //     .where('password', isEqualTo: password)
    //     .get();
    // if (querySnapshot.docs.isEmpty) {
    //   context.loaderOverlay.hide();
    //   Message.show(context: context, message: 'Phone or password is incorrect');
    // } else {
    //   Map<String, dynamic> userData =
    //       querySnapshot.docs.first.data() as Map<String, dynamic>;
    //   UserModel userModel = UserModel.fromJson(userData);
    //   BlocProvider.of<UserBloc>(context).add(SetUSer(userModel: userModel));
    //   context.loaderOverlay.hide();
    //   Message.show(
    //       context: context, message: 'Login successful', isSuccess: true);
    //   Navigator.pushReplacement(
    //       context,
    //       MaterialPageRoute(
    //         builder: (context) => const MyBottomNavigationBar(),
    //       ));
    // }
  }

  void _onLoginAdmin() async {
    String email = _textEditingControllerEmail.text.trim();
    // String phone = _textEditingControllerPhone.text.trim();
    String password = _textEditingControllerPassword.text.trim();
    if (email.isEmpty || password.isEmpty) {
      return Message.show(context: context, message: 'Data can not be blank');
    }
    context.loaderOverlay.show();
    ResponseModel? responseModel = await ApiService.postData(
        "${LinkApi.domain}/v1/api/admin/sign_in",
        {
          "email": email,
          "password": password,
          // "phone_number":phone,
        },
        true);
    context.loaderOverlay.hide();
    if (responseModel?.code == 200) {
      Message.show(
          context: context, message: responseModel?.message, isSuccess: true);
      UserModel userModel = UserModel.fromJson(responseModel?.data);
      setToken(userModel.token);
      BlocProvider.of<UserBloc>(context).add(SetUSer(userModel: userModel.copyWith(role: 1)));
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => const MyBottomNavigationBar(),
          ));
    } else {
      Message.show(context: context, message: responseModel?.message);
    }
  }

  void _resetPassword() async {
    String newPassword = _textEditingControllerNewPassword.text.trim();
    if (newPassword.length < 6) {
      Message.show(context: context, message: "Password minimum 6 characters");
      return;
    }
    DocumentReference userRef = FirebaseFirestore.instance
        .collection('users')
        .doc(_textEditingControllerPhoneForgotPass.text.trim());
    userRef.update({"password": newPassword}).then((value) {
      Navigator.pop(context);
      Message.show(
          context: context,
          message: "Reset password successfully",
          isSuccess: true);
    }).catchError((error) {
      Message.show(context: context, message: "Error: $error");
    });
  }

  void _verifyPhoneNumber() async {
    String phone = _textEditingControllerPhoneForgotPass.text.trim();
    if (phone.isEmpty) {
      Message.show(context: context, message: "Phone number can not be blank");
      return;
    }
    if (!phone.isPhoneNumber()) {
      Message.show(
          context: context,
          message: "The phone number is not in the correct format");
      return;
    }
    DocumentReference userRef =
        FirebaseFirestore.instance.collection('users').doc(phone);
    DocumentSnapshot userSnapshot = await userRef.get();
    if (!userSnapshot.exists) {
      Message.show(context: context, message: "Phone number is not registered");
      return;
    }
    await _auth.verifyPhoneNumber(
      phoneNumber:
          convertGlobalPhone(_textEditingControllerPhoneForgotPass.text.trim()),
      verificationCompleted: (PhoneAuthCredential credential) async {
        // This callback is used when the verification is completed automatically.
      },
      verificationFailed: (FirebaseAuthException e) {
        if (e.code == 'invalid-phone-number') {
          Message.show(context: context, message: "Invalid phone number");
        } else {
          Message.show(context: context, message: "Authentication failed");
        }
      },
      codeSent: (String verificationId, int? resendToken) {
        setState(() {
          _verificationId = verificationId;
        });
        Navigator.pop(context);
        Message.show(
            context: context,
            message: "The otp code has been sent",
            isSuccess: true);
        _onShowDialogEnterOtp();
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        setState(() {
          _verificationId = verificationId;
        });
      },
    );
  }

  void _verifyOTP() async {
    if (_otpCode == null) {
      Message.show(context: context, message: "Please enter the otp code");
      return;
    }
    try {
      final PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: _verificationId!,
        smsCode: _otpCode!,
      );

      await _auth.signInWithCredential(credential);
      Navigator.pop(context);
      Message.show(
          context: context,
          message: "Authentication successful",
          isSuccess: true);
      _onShowDialogResetPassword();
    } catch (e) {
      Message.show(context: context, message: "OTP code is incorrect");
    }
  }

  void _onShowDialogResetPassword() {
    showDialog(
        context: context,
        builder: (context) => MyDialog(
              title: "Reset password",
              onConfirm: _resetPassword,
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  MyTextField(
                    title: "New password",
                    keyboardType: TextInputType.phone,
                    hind: "Enter your new password",
                    controller: _textEditingControllerNewPassword,
                  )
                ],
              ),
            ));
  }

  void _onShowDialogEnterPhone() {
    showDialog(
        context: context,
        builder: (context) => MyDialog(
              title: "Forgot password",
              onConfirm: _verifyPhoneNumber,
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  MyTextField(
                    title: "Phone number",
                    keyboardType: TextInputType.phone,
                    hind: "Enter your phone number",
                    controller: _textEditingControllerPhoneForgotPass,
                  )
                ],
              ),
            ));
  }

  void _onShowDialogEnterOtp() {
    showDialog(
        context: context,
        builder: (context) => MyDialog(
              title: "Verify OTP",
              onConfirm: _verifyOTP,
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  MyOtpInput(
                    onCompletedOtp: (String otp) {
                      _otpCode = otp;
                    },
                  )
                ],
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Welcome!",
              style: textBold36,
            ),
            Text("Login to your account.",
                style: textRegular17.copyWith(color: MyColors.titleHind)),
            const SizedBox(
              height: 36,
            ),
            MyTextField(
              title: "Email",
              hind: "Enter your email",
              keyboardType: TextInputType.emailAddress,
              controller: _textEditingControllerEmail,
            ),
            // const SizedBox(
            //   height: 24,
            // ),
            // MyTextField(
            //   title: "Phone number",
            //   hind: "Enter your phone number",
            //   keyboardType: TextInputType.phone,
            //   controller: _textEditingControllerPhone,
            // ),
            const SizedBox(
              height: 24,
            ),
            MyTextField(
              title: "Password",
              hind: "Enter your password",
              isPassword: true,
              keyboardType: TextInputType.visiblePassword,
              controller: _textEditingControllerPassword,
            ),
            const SizedBox(
              height: 12,
            ),
            Align(
                alignment: Alignment.centerRight,
                child: InkWell(
                  onTap: _onShowDialogEnterPhone,
                  child: Text(
                    "Forgot password?",
                    style: textBold13.copyWith(
                        decoration: TextDecoration.underline),
                  ),
                )),
            const SizedBox(
              height: 24,
            ),

            Row(children: [
              Expanded(
                child: MyButtonFill(
                  title: "Login",
                  onPressed: _onLogin,
                ),
              ),
              const SizedBox(width: 10,),
              Expanded(
                child: MyButtonFill(
                  title: "Login Admin",
                  onPressed: _onLoginAdmin,
                  color: Colors.blueGrey,
                ),
              ),
            ],)
            // Padding(
            //   padding: const EdgeInsets.symmetric(horizontal: 48),
            //   child: MyButtonFill(
            //     title: "Login",
            //     onPressed: _onLogin,
            //   ),
            // ),
            // const SizedBox(
            //   height: 24,
            // ),
            // Padding(
            //   padding: const EdgeInsets.symmetric(horizontal: 48),
            //   child: MyButtonFill(
            //     title: "Login",
            //     onPressed: _onLogin,
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
