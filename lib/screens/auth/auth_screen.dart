import 'package:flutter/material.dart';
import 'package:food_app/screens/auth/login_view.dart';
import 'package:food_app/screens/auth/sign_up_view.dart';
import 'package:food_app/styles/my_colors.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({super.key});

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen>
    with SingleTickerProviderStateMixin {
  static const List<Tab> _myTabs = <Tab>[
    Tab(icon: Text("Login")),
    Tab(icon: Text("Sign Up")),
  ];
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            controller: _tabController,
            indicatorColor: MyColors.black,
            unselectedLabelColor: MyColors.disable,
            labelColor: MyColors.black,
            tabs: _myTabs,
          ),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [
            const LoginView(),
            SignUpView(
              onSuccess: () => setState(() {
                _tabController.index = 0;
              }),
            ),
          ],
        ),
      ),
    );
  }
}
