import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_app/blocs/user_bloc/user_bloc.dart';
import 'package:food_app/models/user_model.dart';
import 'package:food_app/styles/font_styles.dart';
import 'package:food_app/styles/my_colors.dart';
import 'package:food_app/ultils/extensions.dart';
import 'package:food_app/ultils/general.dart';
import 'package:food_app/widgets/my_button_fill.dart';

import '../models/product_model.dart';
import '../ultils/message.dart';

class DetailProductScreen extends StatelessWidget {
  const DetailProductScreen({super.key, required this.productModel});

  final ProductModel productModel;

  void _onAddToCart(BuildContext context) async {
    UserModel userModel = context.read<UserBloc>().state.userModel!;
    DocumentReference cartRef =
        FirebaseFirestore.instance.collection('carts').doc(userModel.id);

    DocumentSnapshot cartSnapshot = await cartRef.get();

    if (cartSnapshot.exists) {
      Map<String, dynamic> cartData =
          cartSnapshot.data() as Map<String, dynamic>;
      Map<String, dynamic> userCart = cartData[productModel.id] ?? {};
      if (cartData.containsKey(productModel.id)) {
        // Tăng quantity lên 1 nếu sản phẩm đã tồn tại
        userCart['quantity'] = (int.parse(userCart['quantity']) + 1).toString();
      } else {
        // Thêm sản phẩm mới vào giỏ hàng nếu chưa tồn tại
        userCart = {
          'id': productModel.id,
          'product': productModel.toJson(),
          'quantity': '1',
        };
      }

      // Cập nhật giỏ hàng trong Firestore
      cartRef.update({
        productModel.id!: userCart,
      }).then((_) {
        Message.show(
            context: context, message: "Add to cart success", isSuccess: true);
      }).catchError((error) {
        Message.show(context: context, message: "Error: $error");
      });
    } else {
      // Nếu giỏ hàng chưa tồn tại, tạo mới giỏ hàng
      Map<String, dynamic> newCart = {
        productModel.id!: {
          'id': productModel.id,
          'product': productModel.toJson(),
          'quantity': '1',
        }
      };
      setData(
        tableName: "carts",
        id: userModel.id!,
        data: newCart,
        onSuccess: () {
          Message.show(
              context: context,
              message: "Add to cart success",
              isSuccess: true);
        },
        onError: (String message) {
          Message.show(context: context, message: "Error: $message");
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(productModel.productName!),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AspectRatio(
                aspectRatio: 2 / 1,
                child: Container(
                    alignment: Alignment.topCenter,
                    child: Image.network(productModel.image ?? ""))),
            const SizedBox(
              height: 24,
            ),
            Text(
              productModel.productName!,
              style: textBold24,
            ),
            const SizedBox(
              height: 12,
            ),
            Text(
              productModel.price!.toString().formatPrice(),
              style: textBold14,
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              productModel.description!,
              style: textRegular14.copyWith(color: MyColors.titleHind),
            ),
            const Spacer(),
            MyButtonFill(
              title: "Add to Cart",
              onPressed: () => _onAddToCart(context),
            )
          ],
        ),
      ),
    );
  }
}
