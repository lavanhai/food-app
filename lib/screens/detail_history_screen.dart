import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_app/blocs/user_bloc/user_bloc.dart';
import 'package:food_app/models/user_model.dart';
import 'package:food_app/ultils/extensions.dart';
import 'package:food_app/ultils/general.dart';

import '../models/order_model.dart';
import '../styles/font_styles.dart';
import '../styles/my_colors.dart';

class DetailHistoryScreen extends StatelessWidget {
  const DetailHistoryScreen({super.key, required this.orderModel});

  final OrderModel orderModel;

  @override
  Widget build(BuildContext context) {
    UserModel userModel = context.read<UserBloc>().state.userModel!;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Detail order"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _ItemRow(
              title: "Code order",
              value: orderModel.id,
            ),
            const SizedBox(height: 4),
            _ItemRow(
              title: "Purchase time",
              value: formatDateTime(orderModel.paymentTime),
            ),
            const SizedBox(height: 4),
            _ItemRow(
              title: "Address",
              value: userModel.address!,
            ),
            const SizedBox(height: 4),
            _ItemRow(
              title: "Total pay",
              value: orderModel.totalPayment,
              valueColor: MyColors.orange,
            ),
            const SizedBox(height: 4),
            Text(
              "Products:",
              style: textRegular14.copyWith(color: MyColors.titleHind),
            ),
            const SizedBox(
              height: 16,
            ),
            ListView.builder(
              shrinkWrap: true,
              itemCount: orderModel.products.length,
              itemBuilder: (context, index) {
                return _ItemProduct(
                  productOrder: orderModel.products[index],
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class _ItemRow extends StatelessWidget {
  const _ItemRow(
      {super.key, required this.title, required this.value, this.valueColor});

  final String title, value;
  final Color? valueColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "$title: ",
          style: textRegular14.copyWith(color: MyColors.titleHind),
        ),
        Expanded(
          child: Text(
            value,
            maxLines: 2,
            textAlign: TextAlign.right,
            style: textBold14.copyWith(color: valueColor),
          ),
        ),
      ],
    );
  }
}

class _ItemProduct extends StatelessWidget {
  const _ItemProduct({super.key, required this.productOrder});

  final ProductOrder productOrder;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16),
      child: Row(
        children: [
          SizedBox(
            width: 100,
            height: 80,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Image.network(
                  productOrder.product.image,
                  fit: BoxFit.cover,
                )),
          ),
          const SizedBox(
            width: 16,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  productOrder.product.productName,
                  style: textBold18.copyWith(height: 1),
                  maxLines: 2,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  "Quantity: ${productOrder.quantity}",
                  style: textBold14,
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  productOrder.product.price.formatPrice(),
                  style: textBold14.copyWith(color: MyColors.orange),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
