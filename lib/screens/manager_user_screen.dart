import 'package:flutter/material.dart';
import 'package:food_app/models/item_user_model.dart';
import 'package:food_app/screens/add_user_screen.dart';
import 'package:food_app/screens/edit_user_screen.dart';
import 'package:food_app/screens/modals/my_dialog.dart';
import 'package:food_app/services/api_service.dart';
import 'package:food_app/services/link_api.dart';
import 'package:food_app/styles/font_styles.dart';
import 'package:loader_overlay/loader_overlay.dart';
import '../models/response_model.dart';
import '../ultils/message.dart';

class ManagerUserScreen extends StatefulWidget {
  const ManagerUserScreen({super.key});

  @override
  State<ManagerUserScreen> createState() => _ManagerUserScreenState();
}

class _ManagerUserScreenState extends State<ManagerUserScreen> {
  List<ItemUserModel> _listItemUserModel = [];

  @override
  void initState() {
    // TODO: implement initState
    _getDataAllUser();
    super.initState();
  }

  void _getDataAllUser() async {
    context.loaderOverlay.show();
    ResponseModel? responseModel = await ApiService.fetchData(
        "${LinkApi.domain}/v1/api/admin/member_view");
    context.loaderOverlay.hide();
    if (responseModel?.code == 200) {
      List<dynamic> data = responseModel?.data;
      _listItemUserModel = data.map((e) => ItemUserModel.fromJson(e)).toList();
      setState(() {});
    }
  }

  void _handleDeleteUser(ItemUserModel itemUserModel) async {
    context.loaderOverlay.show();
    ResponseModel? responseModel = await ApiService.deleteData(
        "${LinkApi.domain}/v1/api/admin/member_delete/${itemUserModel.email}");
    context.loaderOverlay.hide();
    if (responseModel?.code == 200) {
      Navigator.pop(context);
      _getDataAllUser();
      Message.show(
          context: context, message: responseModel?.message, isSuccess: true);
    } else {
      Message.show(context: context, message: responseModel?.message);
    }
  }

  void _showPoupDelete(ItemUserModel itemUserModel) {
    showDialog(
      context: context,
      builder: (context) => MyDialog(
        onConfirm: () => _handleDeleteUser(itemUserModel),
        title: "Delete user",
        content: Text("Are you sure you want to delete?"),
      ),
    );
  }

  void _onEditUser(ItemUserModel itemUserModel) async{
   bool? isSuccess = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              EditUserScreen(itemUserModel: itemUserModel),
        ));

   if(isSuccess == true){
     _getDataAllUser();
   }
  }

  void _handleAddUser() async{
    bool? isSuccess = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              const AddUserScreen(),
        ));

    if(isSuccess == true){
      _getDataAllUser();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Manager Users"),
        actions: [
          IconButton(onPressed: _handleAddUser, icon: const Icon(Icons.add))
        ],
      ),
      body: ListView.builder(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        shrinkWrap: true,
        itemCount: _listItemUserModel.length,
        itemBuilder: (context, index) {
          ItemUserModel itemUserModel = _listItemUserModel[index];
          return GestureDetector(
            onTap: () => _onEditUser(itemUserModel),
            child: Card(
              margin: const EdgeInsets.only(bottom: 16),
              child: Padding(
                padding: const EdgeInsets.only(left: 16, top: 16, bottom: 16),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _ItemRow(
                            title: "ID",
                            value: itemUserModel.userId,
                          ),
                          _ItemRow(
                            title: "Name",
                            value: itemUserModel.name,
                          ),
                          _ItemRow(title: "Email", value: itemUserModel.email),
                          _ItemRow(
                            title: "Phone number",
                            value: itemUserModel.phoneNumber,
                          ),
                          _ItemRow(
                            title: "Address",
                            value: itemUserModel.address,
                          ),
                          _ItemRow(
                            title: "Role",
                            value: itemUserModel.role,
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    IconButton(
                        onPressed: () => _showPoupDelete(itemUserModel),
                        icon: const Icon(
                          Icons.delete_forever,
                          color: Colors.red,
                        ))
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

class _ItemRow extends StatelessWidget {
  const _ItemRow({super.key, required this.title, this.value});

  final String title;
  final String? value;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          "$title: ",
          style: textBold14,
        ),
        Expanded(child: Text(value ?? "value", style: textRegular14)),
      ],
    );
  }
}
