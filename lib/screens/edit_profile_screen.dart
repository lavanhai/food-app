import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_app/blocs/user_bloc/user_bloc.dart';
import 'package:food_app/services/api_service.dart';
import 'package:food_app/services/link_api.dart';

import '../models/response_model.dart';
import '../models/user_model.dart';
import '../ultils/message.dart';
import '../widgets/my_button_fill.dart';
import '../widgets/my_text_field.dart';

class EditProfileScreen extends StatefulWidget {
  const EditProfileScreen({super.key, required this.userModel});

  final UserModel userModel;

  @override
  State<EditProfileScreen> createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  late final TextEditingController _textEditingControllerPhone;
  late final TextEditingController _textEditingControllerFullName;
  late final TextEditingController _textEditingControllerEmail;
  late final TextEditingController _textEditingControllerAddress;

  @override
  void initState() {
    // TODO: implement initState
    _textEditingControllerPhone =
        TextEditingController(text: widget.userModel.phone);
    _textEditingControllerFullName =
        TextEditingController(text: widget.userModel.fullName);
    _textEditingControllerEmail =
        TextEditingController(text: widget.userModel.email);
    _textEditingControllerAddress =
        TextEditingController(text: widget.userModel.address);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _textEditingControllerPhone.dispose();
    _textEditingControllerFullName.dispose();
    _textEditingControllerEmail.dispose();
    _textEditingControllerAddress.dispose();
    super.dispose();
  }

  void _onSave() async{
    String fullName = _textEditingControllerFullName.text.trim();
    String email = _textEditingControllerEmail.text.trim();
    String phone = _textEditingControllerPhone.text.trim();

    if (fullName.isEmpty || email.isEmpty || phone.isEmpty) {
      return Message.show(context: context, message: 'Data can not be blank');
    }
    UserModel userModelNew = widget.userModel
        .copyWith(fullName: fullName, email: email, phone: phone);
    ResponseModel? responseModel = await ApiService.patchData("${LinkApi.domain}/v1/api/users/update/${widget.userModel.id}", {
      "name": fullName,
      "email": email,
      "phone_number": phone,
      "telegram": "telegram_user",
      "sex": "female",
      "birth_date": "10-09-1997"
    });
    if(responseModel?.code == 200){
          Navigator.pop(context);
          BlocProvider.of<UserBloc>(context)
              .add(SetUSer(userModel: userModelNew));
          return Message.show(
              context: context, message: responseModel?.message, isSuccess: true);
    }else{
      return Message.show(
          context: context, message: responseModel?.message);
    }
    // setData(
    //   id: widget.userModel.id!,
    //   tableName: "users",
    //   data: userModelNew.toJson(),
    //   onSuccess: () {
    //     Navigator.pop(context);
    //     BlocProvider.of<UserBloc>(context)
    //         .add(SetUSer(userModel: userModelNew));
    //     return Message.show(
    //         context: context, message: 'Update successful', isSuccess: true);
    //   },
    //   onError: (String message) {
    //     return Message.show(context: context, message: 'Error: $message');
    //   },
    // );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit profile"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyTextField(
                title: "Phone number",
                hind: "Enter your phone number",
                keyboardType: TextInputType.phone,
                // readOnly: true,
                controller: _textEditingControllerPhone,
              ),
              const SizedBox(
                height: 24,
              ),
              MyTextField(
                title: "Full name",
                hind: "Enter your full name",
                controller: _textEditingControllerFullName,
              ),
              const SizedBox(
                height: 24,
              ),
              MyTextField(
                title: "Email",
                hind: "Enter your email",
                controller: _textEditingControllerEmail,
              ),
              const SizedBox(
                height: 24,
              ),
              // MyTextField(
              //   title: "Address",
              //   hind: "Enter your address",
              //   controller: _textEditingControllerAddress,
              // ),
              // const SizedBox(
              //   height: 24,
              // ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 48),
                child: MyButtonFill(
                  title: "Save",
                  onPressed: _onSave,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
