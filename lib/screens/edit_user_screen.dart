import 'package:flutter/material.dart';
import 'package:food_app/widgets/my_button_fill.dart';
import 'package:food_app/widgets/my_text_field.dart';
import 'package:loader_overlay/loader_overlay.dart';

import '../models/item_user_model.dart';
import '../models/response_model.dart';
import '../services/api_service.dart';
import '../services/link_api.dart';
import '../styles/font_styles.dart';
import '../ultils/message.dart';

class EditUserScreen extends StatefulWidget {
  const EditUserScreen({super.key, required this.itemUserModel});

  final ItemUserModel itemUserModel;

  @override
  State<EditUserScreen> createState() => _EditUserScreenState();
}

class _EditUserScreenState extends State<EditUserScreen> {
  String _role = "users";
  late final TextEditingController _textEditingControllerName;
  late final TextEditingController _textEditingControllerPhone;
  late final TextEditingController _textEditingControllerEmail;
  late final TextEditingController _textEditingControllerAddress;

  @override
  void initState() {
    // TODO: implement initState
    _role = widget.itemUserModel.role ?? 'users';
    _textEditingControllerName =
        TextEditingController(text: widget.itemUserModel.name);
    _textEditingControllerPhone =
        TextEditingController(text: widget.itemUserModel.phoneNumber);
    _textEditingControllerEmail =
        TextEditingController(text: widget.itemUserModel.email);
    _textEditingControllerAddress =
        TextEditingController(text: widget.itemUserModel.address);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _textEditingControllerName.dispose();
    _textEditingControllerPhone.dispose();
    _textEditingControllerEmail.dispose();
    _textEditingControllerAddress.dispose();
    super.dispose();
  }

  void _handleSave() async {
    String name = _textEditingControllerName.text.trim();
    String phone = _textEditingControllerPhone.text.trim();
    String email = _textEditingControllerEmail.text.trim();
    String address = _textEditingControllerAddress.text.trim();

    context.loaderOverlay.show();
    ResponseModel? responseModel = await ApiService.patchData(
        "${LinkApi.domain}/v1/api/admin/member_edit/${widget.itemUserModel.userId}",
        {
          "name": name,
          "email": email,
          "phone_number": phone,
          "address": address,
          "role": _role
        });
    context.loaderOverlay.hide();
    if (responseModel?.code == 200) {
      Navigator.pop(context,true);
      Message.show(
          context: context, message: responseModel?.message, isSuccess: true);
    } else {
      Message.show(
          context: context, message: responseModel?.message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit user"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: SingleChildScrollView(
          child: Column(
            children: [
              MyTextField(
                title: "Email",
                hind: "Enter your email",
                keyboardType: TextInputType.emailAddress,
                controller: _textEditingControllerEmail,
              ),
              const SizedBox(
                height: 16,
              ),
              MyTextField(
                title: "Name",
                hind: "Enter your name",
                controller: _textEditingControllerName,
              ),
              const SizedBox(
                height: 16,
              ),
              MyTextField(
                title: "Phone number",
                hind: "Enter your phone number",
                keyboardType: TextInputType.phone,
                controller: _textEditingControllerPhone,
              ),
              const SizedBox(
                height: 16,
              ),
              MyTextField(
                title: "Address",
                hind: "Enter your address",
                controller: _textEditingControllerAddress,
              ),
              const SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  Text(
                    "Role: ",
                    style: textRegular17,
                  ),
                  Expanded(
                    child: Row(
                      children: [
                        Expanded(
                          child: RadioListTile<String>(
                            title: const Text('User'),
                            value: "users",
                            groupValue: _role,
                            onChanged: (String? value) {
                              setState(() {
                                _role = value!;
                              });
                            },
                          ),
                        ),
                        Expanded(
                          child: RadioListTile<String>(
                            title: const Text('Admin'),
                            value: "admin",
                            groupValue: _role,
                            onChanged: (String? value) {
                              setState(() {
                                _role = value!;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 24,
              ),
              MyButtonFill(
                title: "Save",
                onPressed: _handleSave,
              )
            ],
          ),
        ),
      ),
    );
  }
}
