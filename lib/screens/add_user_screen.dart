import 'package:flutter/material.dart';
import 'package:food_app/models/response_model.dart';
import 'package:food_app/services/api_service.dart';
import 'package:food_app/services/link_api.dart';
import 'package:food_app/widgets/my_button_fill.dart';
import 'package:food_app/widgets/my_text_field.dart';
import 'package:loader_overlay/loader_overlay.dart';

import '../models/item_user_model.dart';
import '../styles/font_styles.dart';
import '../ultils/message.dart';

class AddUserScreen extends StatefulWidget {
  const AddUserScreen({super.key,l});

  @override
  State<AddUserScreen> createState() => _AddUserScreenState();
}

class _AddUserScreenState extends State<AddUserScreen> {
  String _role = "users";
  late final TextEditingController _textEditingControllerName;
  late final TextEditingController _textEditingControllerPhone;
  late final TextEditingController _textEditingControllerEmail;
  late final TextEditingController _textEditingControllerAddress;
  late final TextEditingController _textEditingControllerPass;

  @override
  void initState() {
    // TODO: implement initState
    _textEditingControllerName =
        TextEditingController();
    _textEditingControllerPhone =
        TextEditingController();
    _textEditingControllerEmail =
        TextEditingController();
    _textEditingControllerAddress =
        TextEditingController();
    _textEditingControllerPass =
        TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _textEditingControllerName.dispose();
    _textEditingControllerPhone.dispose();
    _textEditingControllerEmail.dispose();
    _textEditingControllerAddress.dispose();
    _textEditingControllerPass.dispose();
    super.dispose();
  }

  void _handleSave() async{
    String name = _textEditingControllerName.text.trim();
    String phone = _textEditingControllerPhone.text.trim();
    String email = _textEditingControllerEmail.text.trim();
    String address = _textEditingControllerAddress.text.trim();
    String pass = _textEditingControllerPass.text.trim();

    context.loaderOverlay.show();
    ResponseModel? responseModel  = await ApiService.postData("${LinkApi.domain}/v1/api/admin/member_add", {
      "name": name,
      "pass_word": pass,
      "email": email,
      "phone_number": phone,
      "address": address,
      "role": _role
    });
    context.loaderOverlay.hide();
    if (responseModel?.code == 200) {
      Navigator.pop(context,true);
      Message.show(
          context: context, message: responseModel?.message, isSuccess: true);
    } else {
      Message.show(
          context: context, message: responseModel?.message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add user"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: SingleChildScrollView(
          child: Column(
            children: [
              MyTextField(
                title: "Email",
                hind: "Enter your email",
                keyboardType: TextInputType.emailAddress,
                controller: _textEditingControllerEmail,
              ),
              const SizedBox(
                height: 16,
              ),
              MyTextField(
                title: "Name",
                hind: "Enter your name",
                controller: _textEditingControllerName,
              ),
              const SizedBox(
                height: 16,
              ),
              MyTextField(
                title: "Phone number",
                hind: "Enter your phone number",
                keyboardType: TextInputType.phone,
                controller: _textEditingControllerPhone,
              ),
              const SizedBox(
                height: 16,
              ),
              MyTextField(
                title: "Address",
                hind: "Enter your address",
                controller: _textEditingControllerAddress,
              ),
              const SizedBox(
                height: 16,
              ),
              MyTextField(
                title: "Password",
                hind: "Enter your password",
                isPassword: true,
                controller: _textEditingControllerPass,
              ),
              const SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  const Text(
                    "Role: ",
                    style: textRegular17,
                  ),
                  Expanded(
                    child: Row(
                      children: [
                        Expanded(
                          child: RadioListTile<String>(
                            title: const Text('User'),
                            value: "users",
                            groupValue: _role,
                            onChanged: (String? value) {
                              setState(() {
                                _role = value!;
                              });
                            },
                          ),
                        ),
                        Expanded(
                          child: RadioListTile<String>(
                            title: const Text('Admin'),
                            value: "admin",
                            groupValue: _role,
                            onChanged: (String? value) {
                              setState(() {
                                _role = value!;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 24,
              ),
              MyButtonFill(
                title: "Save",
                onPressed: _handleSave,
              )
            ],
          ),
        ),
      ),
    );
  }
}
