import 'package:flutter/material.dart';
import 'package:loader_overlay/loader_overlay.dart';

import '../models/product_model.dart';
import '../ultils/general.dart';
import '../ultils/message.dart';
import '../widgets/my_button_fill.dart';
import '../widgets/my_text_field.dart';

class CreateProductScreen extends StatefulWidget {
  const CreateProductScreen({super.key, this.productModel});

  final ProductModel? productModel;

  @override
  State<CreateProductScreen> createState() => _CreateProductScreenState();
}

class _CreateProductScreenState extends State<CreateProductScreen> {
  late final TextEditingController _textEditingControllerName;
  late final TextEditingController _textEditingControllerPrice;
  late final TextEditingController _textEditingControllerImage;
  late final TextEditingController _textEditingControllerDescription;

  @override
  void initState() {
    // TODO: implement initState
    _textEditingControllerName =
        TextEditingController(text: widget.productModel?.productName);
    _textEditingControllerPrice =
        TextEditingController(text: widget.productModel?.price);
    _textEditingControllerImage =
        TextEditingController(text: widget.productModel?.image);
    _textEditingControllerDescription =
        TextEditingController(text: widget.productModel?.description);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _textEditingControllerName.dispose();
    _textEditingControllerPrice.dispose();
    _textEditingControllerImage.dispose();
    _textEditingControllerDescription.dispose();
    super.dispose();
  }

  void _onCreate(BuildContext context) async {
    String uuid = widget.productModel == null ? idGenerator() : widget.productModel!.id!;
    String name = _textEditingControllerName.text.trim();
    String price = _textEditingControllerPrice.text.trim();
    String des = _textEditingControllerDescription.text.trim();
    String image = _textEditingControllerImage.text.trim();

    if (name.isEmpty || price.isEmpty || des.isEmpty || image.isEmpty) {
      return Message.show(context: context, message: "Data cannot be blank");
    }
    context.loaderOverlay.show();
    setData(
      id: uuid,
      tableName: "products",
      data: ProductModel(
              id: uuid,
              price: price,
              description: des,
              productName: name,
              image: image)
          .toJson(),
      onSuccess: () {
        _textEditingControllerName.clear();
        _textEditingControllerPrice.clear();
        _textEditingControllerDescription.clear();
        _textEditingControllerImage.clear();
        context.loaderOverlay.hide();
        if(widget.productModel != null){
          Navigator.pop(context);
        }
        return Message.show(
            context: context,
            message: widget.productModel == null
                ? "Create product success"
                : "Update product success",isSuccess: true);
      },
      onError: (String message) {
        context.loaderOverlay.hide();
        return Message.show(context: context, message: "Error: $message");
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
            widget.productModel == null ? "Create product" : "Update product"),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: SingleChildScrollView(
            child: Column(
              children: [
                MyTextField(
                  title: "Product name",
                  hind: "Enter product name",
                  controller: _textEditingControllerName,
                ),
                const SizedBox(
                  height: 16,
                ),
                MyTextField(
                  title: "Description",
                  hind: "Enter description",
                  maxLine: 4,
                  controller: _textEditingControllerDescription,
                ),
                const SizedBox(
                  height: 16,
                ),
                MyTextField(
                  title: "Price",
                  hind: "Enter price",
                  keyboardType: TextInputType.number,
                  controller: _textEditingControllerPrice,
                ),
                const SizedBox(
                  height: 16,
                ),
                MyTextField(
                  title: "Image",
                  hind: "Enter image link",
                  keyboardType: TextInputType.url,
                  controller: _textEditingControllerImage,
                ),
                const SizedBox(
                  height: 36,
                ),
                MyButtonFill(
                  title: widget.productModel == null ? "Create" : "Save",
                  onPressed: () => _onCreate(context),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
