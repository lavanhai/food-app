import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_app/ultils/extensions.dart';

import '../../blocs/user_bloc/user_bloc.dart';
import '../../models/coupon_model.dart';
import '../../models/user_model.dart';
import '../../widgets/coupon_card.dart';

class ModalVoucher extends StatelessWidget {
  const ModalVoucher({super.key, this.onSelected, this.voucherSelected});
  final CouponModel? voucherSelected;
  final Function(CouponModel?)? onSelected;

  @override
  Widget build(BuildContext context) {
    UserModel userModel = context.read<UserBloc>().state.userModel!;
    final Stream<QuerySnapshot> couponsStream = FirebaseFirestore.instance
        .collection('coupons')
        .snapshots();
    return SizedBox(
        height: 400,
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: StreamBuilder<QuerySnapshot>(
              stream: couponsStream,
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError) {
                  return const Text('Something went wrong');
                }

                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Text("Loading");
                }

                final filteredDocs =
                    snapshot.data!.docs.where((DocumentSnapshot document) {
                  Map<String, dynamic> data =
                      document.data()! as Map<String, dynamic>;
                  return data["users"]?[userModel.phone] == true;
                }).toList();

                return ListView(
                  shrinkWrap: true,
                  children: filteredDocs.map((DocumentSnapshot document) {
                    Map<String, dynamic> data =
                        document.data()! as Map<String, dynamic>;
                    CouponModel couponModel = CouponModel.fromJson(data);
                    bool isSelected = voucherSelected?.id == couponModel.id;
                    return CouponCard(
                        isAdmin: false,
                        onGetVoucher: (){
                          Navigator.pop(context);
                          onSelected?.call(isSelected ? null : couponModel);
                        },
                        buttonName: isSelected ? "Uncheck" :"Apply",
                        discount: couponModel.discount!.formatAmount(),
                        quantity: couponModel.quantity!);
                  }).toList(),
                );
              },
            )));
  }
}
