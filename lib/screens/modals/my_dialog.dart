import 'package:flutter/material.dart';

class MyDialog extends StatelessWidget {
  const MyDialog(
      {super.key,
      required this.title,
      this.onConfirm,
      this.content,
      this.textConfirm,
      this.textCancel,
      this.onCancel});

  final String title;
  final String? textConfirm, textCancel;
  final Widget? content;
  final VoidCallback? onConfirm, onCancel;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: content,
      surfaceTintColor: Colors.white,
      actions: <Widget>[
        TextButton(
          onPressed: onCancel ?? () => Navigator.pop(context, 'Cancel'),
          child: Text(textCancel ?? 'Cancel'),
        ),
        TextButton(
          onPressed: onConfirm,
          child: Text(textConfirm ?? 'OK'),
        ),
      ],
    );
  }
}
