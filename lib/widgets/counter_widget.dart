import 'package:flutter/material.dart';

class CounterWidget extends StatefulWidget {
  const CounterWidget({super.key, this.onChange, this.value});
  final Function(int)? onChange;
  final int? value;

  @override
  CounterWidgetState createState() => CounterWidgetState();
}

class CounterWidgetState extends State<CounterWidget> {
  late int _counter;

  @override
  void initState() {
    // TODO: implement initState
    _counter = widget.value ?? 1;
    super.initState();
  }

  void _increment() {
    if (_counter + 1 < 100) {
      setState(() {
        _counter++;
      });
      widget.onChange?.call(_counter);
    }
  }

  void _decrement() {
    if (_counter - 1 > 0) {
      setState(() {
        _counter--;
      });
      widget.onChange?.call(_counter);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        InkWell(
          onTap: _decrement,
          child: Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(4),
              ),
              child: const Icon(Icons.remove, color: Colors.black45)),
        ),
        SizedBox(
          width: 40,
          child: Text(
            '$_counter',
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 14),
          ),
        ),
        InkWell(
          onTap: _increment,
          child: Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(4),
              ),
              child: const Icon(Icons.add, color: Colors.black45)),
        ),
      ],
    );
  }
}
