import 'package:flutter/material.dart';
import 'package:food_app/styles/my_colors.dart';

import '../styles/font_styles.dart';

class MyButtonFill extends StatelessWidget {
  const MyButtonFill(
      {super.key, this.onPressed, required this.title, this.color, this.textStyle});

  final Function()? onPressed;
  final String title;
  final Color? color;
  final TextStyle? textStyle;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 58,
      width: double.infinity,
      child: ElevatedButton(
          onPressed: onPressed,
          style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(14)
                  )
              ),
              backgroundColor: MaterialStatePropertyAll(color ?? MyColors.orange),
              elevation: const MaterialStatePropertyAll(0)),
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: textStyle ?? textMedium18.copyWith(color: Colors.white),
          )),
    );
  }
}
