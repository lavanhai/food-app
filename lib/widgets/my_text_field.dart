import 'package:flutter/material.dart';
import 'package:food_app/styles/font_styles.dart';
import 'package:food_app/styles/my_colors.dart';

class MyTextField extends StatelessWidget {
  const MyTextField(
      {super.key,
      this.title,
      this.hind,
      this.maxLine,
      this.keyboardType,
      this.controller,
      this.isPassword,
      this.readOnly});

  final String? title, hind;
  final int? maxLine;
  final TextInputType? keyboardType;
  final TextEditingController? controller;
  final bool? isPassword, readOnly;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (title != null)
          Text(
            title!,
            style: textRegular17,
          ),
        const SizedBox(
          height: 8,
        ),
        TextField(
          controller: controller,
          minLines: 1,
          readOnly: readOnly ?? false,
          keyboardType: keyboardType,
          maxLines: maxLine ?? 1,
          obscureText: isPassword ?? false,
          style: TextStyle(color: readOnly == true ? MyColors.disable : null),
          decoration: InputDecoration(
            hintText: hind,
            hintStyle: textRegular14.copyWith(color: MyColors.disable),
            filled: true,
            fillColor: Colors.grey[200],
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(14)),
              borderSide: BorderSide.none,
            ),
          ),
        )
      ],
    );
  }
}
