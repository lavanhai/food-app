import 'package:flutter/material.dart';
import 'package:food_app/styles/font_styles.dart';

class CouponCard extends StatelessWidget {
  const CouponCard(
      {super.key,
      required this.discount,
      this.onGetVoucher,
      required this.quantity,
      required this.isAdmin,
      this.onDeleteVoucher, this.disable, this.buttonName});

  final bool isAdmin;
  final bool? disable;
  final String discount, quantity;
  final String? buttonName;
  final VoidCallback? onGetVoucher, onDeleteVoucher;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 7,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: Row(
        children: [
          Container(
            margin: const EdgeInsets.only(left: 16, top: 16, bottom: 16),
            padding: const EdgeInsets.all(16.0),
            decoration: const BoxDecoration(
              color: Color(0xFFFF5722),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0),
                bottomLeft: Radius.circular(8.0),
              ),
            ),
            child: const Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.shopping_bag, color: Colors.white, size: 40),
                SizedBox(height: 8.0),
                Text(
                  'Voucher',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 16, left: 16, bottom: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Discount $discount',
                    style: textBold14,
                  ),
                  const SizedBox(height: 8.0),
                  Text(
                    'Quantity: $quantity',
                    style: textRegular14,
                  ),
                  const SizedBox(height: 8.0),
                  Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 4.0, horizontal: 8.0),
                    decoration: BoxDecoration(
                      border: Border.all(color: const Color(0xFFFF5722)),
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    child: const Text(
                      'App Food',
                      style: TextStyle(
                        color: Color(0xFFFF5722),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(right: 16.0, left: 8),
            child: ElevatedButton(
              onPressed: disable == true ? null : (isAdmin ? onDeleteVoucher : onGetVoucher),
              child: Text(buttonName ?? (isAdmin ? 'Delete' : 'Get')),
            ),
          ),
        ],
      ),
    );
  }
}
