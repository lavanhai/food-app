import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/response_model.dart';
import '../ultils/global.dart';

class ApiService {
  static Future<ResponseModel?> fetchData(String url) async {
    try {
      http.Response response = await http.get(
        Uri.parse(url),
        headers: {'Authorization': 'Bearer $token'},
      ).timeout(
        const Duration(seconds: 10),
        onTimeout: () {
          // Time has run out, do what you wanted to do.
          return http.Response('Error', 408); // Request Timeout response status code
        },
      );
      var source = jsonDecode(utf8.decode(response.bodyBytes));
      ResponseModel? responseModel = ResponseModel.fromJson(source);
      return responseModel;
    } catch (e) {
      throw Exception('Failed to connect to the server');
    }
  }

  static Future<ResponseModel?> postData(String url, Map<String, dynamic> data,
      [bool? tokenIsEmpty]) async {
    try {
      http.Response response = await http.post(
        Uri.parse(url),
        body: json.encode(data),
        headers: tokenIsEmpty == true
            ? {'Content-Type': 'application/json'}
            : {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer $token',
              },
      ).timeout(
        const Duration(seconds: 10),
        onTimeout: () {
          // Time has run out, do what you wanted to do.
          return http.Response('Error', 408); // Request Timeout response status code
        },
      );
      print(response.body);
      var source = jsonDecode(utf8.decode(response.bodyBytes));
      ResponseModel? responseModel = ResponseModel.fromJson(source);
      return responseModel;
      // if (response.statusCode == 200) {
      //   var source = jsonDecode(utf8.decode(response.bodyBytes));
      //   ResponseModel responseModel = ResponseModel.fromJson(source);
      //   return responseModel;
      // }
      // throw Exception( response.reasonPhrase);
    } catch (e) {
      throw Exception('Failed to connect to the server');
    }
  }

  static  Future<ResponseModel?> patchData(String url, Map<String, dynamic> data) async {
    try {
      http.Response response = await http.patch(
        Uri.parse(url),
        body: json.encode(data),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      ).timeout(
        const Duration(seconds: 10),
        onTimeout: () {
          // Time has run out, do what you wanted to do.
          return http.Response('Error', 408); // Request Timeout response status code
        },
      );
      var source = jsonDecode(utf8.decode(response.bodyBytes));
      ResponseModel? responseModel = ResponseModel.fromJson(source);
      return responseModel;
    } catch (e) {
      throw Exception('Failed to connect to the server');
    }
  }

  static Future<ResponseModel?> deleteData(String url, [Map<String, dynamic>? data]) async {
    try {
      http.Response response = await http.delete(
        Uri.parse(url),
        headers: {'Authorization': 'Bearer $token'},
        body: json.encode(data),
      ).timeout(
        const Duration(seconds: 10),
        onTimeout: () {
          // Time has run out, do what you wanted to do.
          return http.Response('Error', 408); // Request Timeout response status code
        },
      );
      var source = jsonDecode(utf8.decode(response.bodyBytes));
      ResponseModel? responseModel = ResponseModel.fromJson(source);
      return responseModel;
    } catch (e) {
      throw Exception('Failed to connect to the server');
    }
  }
}
